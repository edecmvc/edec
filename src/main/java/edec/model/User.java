package edec.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class User {

	private String username;
	private String password;
	private boolean enabled;
	private Set<UserRole> userRole = new HashSet<UserRole>(0);

	public User() {
	}

	public User(String username, String password, boolean enabled) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	public User(String username, String password, boolean enabled, Set<UserRole> userRole) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.userRole = userRole;
	}

	@Id
	@Column(name = "username", unique = true, nullable = false, length = 45)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = false, length = 60)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "enabled", nullable = false)
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade=CascadeType.ALL)
	public Set<UserRole> getUserRole() {
		return this.userRole;
	}

	private Set<Group> groups;

	 
	  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Group.class)
	  @JoinTable(name = "group_joins", joinColumns = { @JoinColumn(name = "username") }, inverseJoinColumns = { @JoinColumn(name = "groupid") })
	public Set<Group> getGroups() {
		return groups;
	}
	  
	private Set<Campaign> joinedCampaigns = new HashSet<>();

	 @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Campaign.class)
	 @JoinTable(name = "campaign_joins", joinColumns = { @JoinColumn(name = "username") }, inverseJoinColumns = { @JoinColumn(name = "CampaignID") })  
	public Set<Campaign> getJoinedCampaigns() {
		return joinedCampaigns;
	}

	public void setJoinedCampaigns(Set<Campaign> joinedCampaigns) {
		this.joinedCampaigns = joinedCampaigns;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}
        
        @Column(name = "realName")
        private String realName;
        
        public String getRealName(){
            return this.realName;
        }
        
        public void setRealName(String _realName){
            this.realName = _realName;
        }
        
        @Column(name = "avatarPath")
        private String avatarPath;
        
        public String getAvatarPath(){
            return this.avatarPath;
        }
        
        public void setAvatarPath(String _avatarPath){
            this.avatarPath = _avatarPath;
        }
}
