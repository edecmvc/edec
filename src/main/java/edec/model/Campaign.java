package edec.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edec.util.JsonDateSerializer;



@Entity
@Table(name = "campaigns")
public class Campaign implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "CampaignID")
	private int campaignID;

	@NotNull
	@Length(min=3,message="Campaign name must be at least 3 characters long.")
	@Column(name = "Name")
	private String name;
        
        @NotNull
	@Column(name = "Creator")
	private String creator;
        
        public String getCreator() {
            return this.creator;
	}

	public void setCreator(String _creator) {
            this.creator = _creator;
	}
        
        @NotNull
	@Column(name = "targetJoins")
	private int targetJoins;
        
        public int getTargetJoins() {
            return this.targetJoins;
	}

	public void setTargetJoins(int _targetJoins) {
            this.targetJoins = _targetJoins;
	}
        
        @NotNull
	@Column(name = "currentJoins")
	private int currentJoins;
        
        public int getcurrentJoins() {
            return this.currentJoins;
	}

	public void setcurrentJoins(int _currentJoins) {
            this.currentJoins = _currentJoins;
	}

	@NotNull
	@Length(min=3,message="Campaign description must be at least 3 characters long")
	@Column(name = "Description")
	private String description;

	@JsonSerialize(using=JsonDateSerializer.class)
	@Column(name = "CreationDate")
	private Date creationDate;
	
	@Column(name="iconPath")
	private String iconPath;
	
	public int getCampaignID() {
		return campaignID;
	}

	public void setCampaignID(int campaignID) {
		this.campaignID = campaignID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public String getCampaignPath() {
		return iconPath;
	}

	public void setCampaignPath(String campaignPath) {
		this.iconPath = campaignPath;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}


	
}
