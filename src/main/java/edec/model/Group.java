package edec.model;


import javax.persistence.*;

@Entity
@Table(name="groups")
public class Group {
	
	@Id
	@Column(name="groupid")
	private int groupid;
	
	@Column(name="creator")
	private String creator;
	
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
        
        @Transient
	private int numberOfMembers;

	public int getNumberOfMembers() {
		return numberOfMembers;
	}

	public void setNumberOfMembers(int numberOfMembers) {
		this.numberOfMembers = numberOfMembers;
	}
        
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;

	public int getGroupid() {
		return groupid;
	}

	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
