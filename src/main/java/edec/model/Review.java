package edec.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edec.util.JsonDateSerializer;
import edec.util.ReviewPK;

@Entity
@IdClass(ReviewPK.class)
@Table(name = "reviews")
public class Review {

    @Id
    @Column(name = "username")
    private String userName;

    @Id
    @Column(name = "ProductID")
    private int productID;

    @NotNull
    @Range(min = 0, max = 5)
    @Column(name = "rating")
    private int rating;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ProductID", insertable = false, updatable = false)
    Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Column(name = "reviewdate")
    @Temporal(TemporalType.DATE)
    private Date reviewDate;

    @NotNull
    @Length(min = 1, max = 512, message = "Review length must be between {2} and {1} characters.")
    @Column(name = "review")
    private String review;

    public String getUserName() {
        return userName;
    }

    @Transient
    private String userImagePath;

    public String getUserImagePath() {
        return userImagePath;
    }

    public void setUserImagePath(String userImagePath) {
        this.userImagePath = userImagePath;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
