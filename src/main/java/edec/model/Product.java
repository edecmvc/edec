package edec.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

@Entity
@Table(name="products")
public class Product {
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@Id
	@Column(name="ProductID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@NotNull(message="Name cannot be empty.")
	@Length(min=3,message="The minimum length of the name must be 3.")
	@Column(name="Name")
	private String name;
	
	@Column(name="Brand")
	@Length(min=1,message="Brand cannot be empty.")
	private String brand;
	
	@Length(min=1,message="Description cannot be empty.")
	@Column(name="Description")
	private String description;
	
	@Range(min=1,message="Price must be greater than 0!")
	@Column(name="Price")
	private double price;
	
	@Length(min=1,message="Composition cannot be empty")
	@Column(name="Composition")
	private String composition;
        
        @Column(name = "iconPath")
        private String iconPath;
        
        public String getIconPath(){
            return this.iconPath;
        }
        
        public void setIconPath(String _iconPath){
            this.iconPath = _iconPath;
        }
	
	
	public String getComposition() {
		return composition;
	}
	public String getDescription() {
		return description;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	
	public double getPrice() {
		return price;
	}
	public void setComposition(String composition) {
		this.composition = composition;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPrice(double price) {
		this.price = price;
	}
        
        
	
	
}
