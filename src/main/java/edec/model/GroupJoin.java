
package edec.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "group_joins")
public class GroupJoin implements Serializable{

    @Column(name = "username")
    private String userName;

    @Column(name = "groupid")
    private int groupid;
    
    @Id
    @Column(name="group_join_id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    public String getUserName() {
        return userName;
    }

    public int getGroupid() {
		return groupid;
	}

	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getGroupID() {
        return groupid;
    }

    public void setGroupID(int groupid) {
        this.groupid = groupid;
    }

}
