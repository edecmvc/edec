package edec.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edec.model.Campaign;
import edec.model.CampaignJoin;
import edec.model.Group;
import edec.model.GroupJoin;
import edec.model.Review;
import java.util.Map;
import java.util.TreeMap;
import org.hibernate.Criteria;

@Repository("DAO")
public class GenericDAOImpl<T> implements GenericDAO<T> {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public int saveEntity(Object t) {
        return (int) sessionFactory.getCurrentSession().save(t);
    }

    @Override
    @Transactional
    public Object getEntity(final Class<T> type, final int id) {

        Object targetEntity = sessionFactory.getCurrentSession().get(type, id);
        return targetEntity;
    }

    private Class<T> type;

    public GenericDAOImpl(Class<T> type) {
        this.type = type;
    }

    public GenericDAOImpl() {

    }

    @Transactional
    @Override
    public void deleteEntity(Class<T> type, int id) {

        Object targetEntity = this.getEntity(type, id);

        if (targetEntity != null) {
            this.sessionFactory.getCurrentSession().delete(targetEntity);
        }
    }

    @Override
    public void updateEntity(Class<T> type, int id) {

        Object targetEntity = this.getEntity(type, id);
        if (targetEntity != null) {
            sessionFactory.getCurrentSession().update(targetEntity);
        }
    }

    @Override
    @Transactional
    public boolean isAlreadyTaken(String name) {

        String hql = "from Campaign c where c.name = :param";
        Campaign targetCampaign = (Campaign) sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("param", name).uniqueResult();
        if (targetCampaign != null) {
            return true;
        }

        return false;
    }

    @Override
    public void saveReview(Review review) {
        // TODO Auto-generated method stub

    }

    @Override
    @Transactional
    public boolean userHasJoined(String username, int campaignID) {
        CampaignJoin entity = (CampaignJoin) sessionFactory.getCurrentSession()
                .createQuery("from CampaignJoin where username = :user and campaignId = :id")
                .setParameter("user", username)
                .setParameter("id", campaignID).uniqueResult();

        if (entity != null) {
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public List<T> getAll(Class type) {
        return sessionFactory.getCurrentSession().createCriteria(type).list();
    }

    @Override
    @Transactional
    public List<Group> getAllGroups() {
        return sessionFactory.getCurrentSession().createCriteria(Group.class).list();
    }

    @Override
    @Transactional
    public boolean userHasJoinedGroup(String username, int groupID) {
        GroupJoin groupJoin = (GroupJoin) sessionFactory.getCurrentSession()
                .createQuery("from GroupJoin where username = :user and groupid = :gid")
                .setParameter("user", username)
                .setParameter("gid", groupID)
                .uniqueResult();
        if (groupJoin != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean userReviewedProduct(int productID) {
        Session session = sessionFactory.getCurrentSession();

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Review review = (Review) session.createQuery("from Review where userName = :usr and productID = :pid")
                .setParameter("usr", currentUser)
                .setParameter("pid", productID)
                .uniqueResult();

        if (review != null) {
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public List<Review> getRandomReviews(int id, int number) {
        List<Review> results = sessionFactory.getCurrentSession().createQuery("from Review where productID = :param order by rand()")
                .setParameter("param", id)
                .setMaxResults(number)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();

        for (Review r : results) {
            String username = r.getUserName();
            String avatarPath = (String) sessionFactory.getCurrentSession().
                    createQuery("select avatarPath from User where username = :usr")
                    .setParameter("usr", username)
                    .uniqueResult();
            r.setUserImagePath(avatarPath);
        }

        return results;
    }

    @Transactional

    @Override

    public int unsubscribeUserFromCampaign(int id) {

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        int count = sessionFactory.getCurrentSession()
                .createQuery("from CampaignJoin where username = :userParam and campaignId = :idParam")
                .setParameter("userParam", currentUser)
                .setParameter("idParam", id)
                .executeUpdate();

        return count;

    }

    @Override

    @Transactional

    public Map<Integer, String> reviewStatistics(int id) {

        String query = "select rating,count(rating) from reviews where ProductID = ? group by rating order by rating;";

        List<Object[]> queryResults = sessionFactory.getCurrentSession().createSQLQuery(query)
                .setParameter(0, id)
                .list();

        Map<Integer, String> results = new TreeMap<>();

        for (int i = 1; i <= 5; ++i) {

            results.put(i, 0 + "");

        }

        for (Object[] o : queryResults) {

            results.put(new Double(o[0].toString()).intValue(), o[1].toString());

        }

        return results;

    }

    @Override
    @Transactional
    public boolean checkIfGroupExists(String name) {

        Group group = (Group) sessionFactory.getCurrentSession().createQuery("from Group where username = :param")
                .setParameter("param", name)
                .uniqueResult();

        if (group != null) {
            return true;
        }

        return false;
    }
}
