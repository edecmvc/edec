package edec.dao;

import edec.model.Review;
import edec.model.User;
import java.util.List;

public interface UserDao {

    User findByUserName(String username);


}
