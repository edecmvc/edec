package edec.dao;

import java.util.List;

import edec.model.Group;
import edec.model.Review;
import java.util.Map;

public interface GenericDAO <T> {
	public int saveEntity(T t);
	public void deleteEntity(final Class<T> type, final int id);
	public void updateEntity(final Class<T> type, final int id);
	public Object getEntity(final Class<T> type, final int id);
	public boolean isAlreadyTaken(String name);
	public void saveReview(Review review);
	boolean userHasJoined(String username, int campaignID);
	public List<T> getAll(Class type);
	public List<Group> getAllGroups();
	boolean userHasJoinedGroup(String username,int groupID);
	boolean userReviewedProduct(int productID);
	List<Review> getRandomReviews(int id,int number);
        int unsubscribeUserFromCampaign(int id);
	Map<Integer, String> reviewStatistics(int id);
        boolean checkIfGroupExists(String name);
}

