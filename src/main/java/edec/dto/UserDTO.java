package edec.dto;

public class UserDTO {
	
	private String username;
	private String password;
        private String realName;
        private String avatarPath;
	private int enabled;
        
        public String getAvatarPath(){
            return this.avatarPath;
        }
        
        public void setAvatarPath(String _avatarPath){
            this.avatarPath = _avatarPath;
        }
        
        public String getRealName(){
            return this.realName;
        }
        
        public void setRealName(String _realName){
            this.realName = _realName;
        }
        
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public UserDTO(){
		
	}
}
