package edec.dto;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edec.util.JsonDateSerializer;

public class CampaignDTO {

	private String name;
	private String description;

	private Date date;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
