package edec.dto;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edec.util.JsonDateSerializer;

public class ReviewDTO {

	private String userName;
	private int productID;
	private int rating;
	private String review;
	
	@JsonSerialize(using = JsonDateSerializer.class)
	private Date reviewDate;
	private String name;

	
	public ReviewDTO(String userName,String productName,int rating,String review,Date reviewDate,int productID){
		this.userName=userName;
		this.name=productName;
		this.rating=rating;
		this.review=review;
		this.reviewDate=reviewDate;
		this.productID= productID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

}
