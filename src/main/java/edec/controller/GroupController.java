package edec.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edec.dao.GenericDAO;
import edec.model.Group;
import edec.model.GroupJoin;
import edec.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GroupController {

    @Autowired
    GenericDAO<Group> service;

    @Autowired
    GenericDAO<GroupJoin> groupJoinService;
    @Autowired
    SessionFactory sessionFactory;
    // This method will get the unsubscribed campaigns and apply relevant
    // filtering
    int rowStart = 0;

    @Transactional
    @RequestMapping(value = "/groups/recommend/{username}")
    public @ResponseBody
    List<Group> recommendGroups(@PathVariable String username) {
        User user = (User) sessionFactory.getCurrentSession().createQuery("from User u where u.username = :user")
                .setParameter("user", username).uniqueResult();

        List<Integer> ids = new ArrayList<Integer>();
        for (Group g : user.getGroups()) {
            ids.add(g.getGroupid());
        }
        Session session = sessionFactory.getCurrentSession();
        List<Integer> results = new ArrayList<>();
        List<String> users = new ArrayList<>();
        if (ids.isEmpty()) {
            return null;
        }
        users = session
                .createQuery(
                        "select userName from GroupJoin where groupid in (:list)")
                .setParameterList("list", ids).list();
        if (users.isEmpty()) {
            return null;
        }
        results = session
                .createQuery(
                        "select g.groupid from GroupJoin g where g.groupid not in (:list) and g.userName in :usr ")
                .setParameterList("usr", users).setParameterList("list", ids).list();
        if (results.isEmpty()) {
            return null;
        }
        List<Group> finalResult = new ArrayList<>();
        finalResult = session
                .createQuery(
                        "from Group g where g.groupid in (:list)")
                .setParameterList("list", results).list();
        System.out.println("");
        List<Group> fewGroups = new ArrayList<>();
        for (int i = rowStart; i < rowStart + 5 && i < finalResult.size(); ++i) {
            fewGroups.add(finalResult.get(i));
        }
        rowStart += 5;
        return fewGroups;
    }

    @RequestMapping(value = "/recommendgroups")
    public String recommendGroups() {
        rowStart = 0;
        return "recommendgroups";
    }

    @Transactional
    @RequestMapping("/rest/groups")
    public @ResponseBody
    List<Group> getAllGroups() {
        List<Group> results = service.getAll(Group.class);
        for (Group g : results) {
            String query = "select count(groupid) from group_joins where groupid = ?";
            int result = new Integer(sessionFactory.getCurrentSession().createSQLQuery(query).setParameter(0, g.getGroupid()).uniqueResult().toString());
            g.setNumberOfMembers(result);
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping("/rest/groups/search")
    @Transactional
    public @ResponseBody
    List<Group> viewGroups(@RequestParam(required = false) String keyword) {

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Group.class);

        if (keyword != null) {
            if (keyword.length() > 1) {
                Disjunction disjunction = Restrictions.disjunction();

                disjunction.add(Restrictions.ilike("creator", keyword, MatchMode.ANYWHERE));
                criteria.add(disjunction);

                if (criteria.list().size() == 0) {
                    disjunction.add(Restrictions.ilike("name", keyword, MatchMode.ANYWHERE));
                    criteria.add(disjunction);
                }
                if (criteria.list().size() == 0) {
                    disjunction.add(Restrictions.ilike("description", keyword, MatchMode.ANYWHERE));
                    criteria.add(disjunction);
                }
            } else {
                return criteria.list();
            }
        }

        List<Group> finalResults = criteria.list();
        for (Group g : finalResults) {
            String query = "select count(groupid) from group_joins where groupid = ?";
            int result = new Integer(sessionFactory.getCurrentSession().createSQLQuery(query).setParameter(0, g.getGroupid()).uniqueResult().toString());
            g.setNumberOfMembers(result);
        }
        return finalResults;
    }

    @RequestMapping("/group/{id}")
    public String groupView(@PathVariable int id, Model model) {

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Group group = (Group) service.getEntity(Group.class, id);
        model.addAttribute("group", group);
        if (service.userHasJoinedGroup(currentUser, id)) {
            model.addAttribute("flag", "true");
        }

        return "groupview";
    }

    @RequestMapping(value = "/joinGroup/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> joinGroup(@PathVariable int id, Model model) {
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (service.userHasJoinedGroup(currentUser, id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            GroupJoin groupJoin = new GroupJoin();
            groupJoin.setUserName(currentUser);
            groupJoin.setGroupID(id);
            groupJoinService.saveEntity(groupJoin);

            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @RequestMapping("/addgroup")
    public String addGroup() {
        return "addgroup";
    }

    @RequestMapping(value = "/rest/addGroup", method = RequestMethod.POST)
    public ResponseEntity<Group> addGroupService(@RequestBody Group group) {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();

       // if (service.checkIfGroupExists(group.getName())) {

            Group newGroup = new Group();
            newGroup.setDescription(group.getDescription());
            newGroup.setName(group.getName());
            newGroup.setCreator(username);

            try {
                service.saveEntity(newGroup);
            } catch (Exception e) {
                return new ResponseEntity<>(newGroup, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(HttpStatus.OK);
    }
}
