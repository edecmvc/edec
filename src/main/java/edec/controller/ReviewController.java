package edec.controller;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edec.dao.GenericDAO;
import edec.model.Group;
import edec.model.Product;
import edec.model.Review;
import edec.model.User;
import edec.util.ReviewForm;
import java.util.ArrayList;
import java.util.Map;

@Controller
public class ReviewController {

    @Autowired
    GenericDAO<Product> service;

    @Autowired
    GenericDAO<Review> reviewService;

    @Autowired
    SessionFactory sessionFactory;

    @Transactional
    @RequestMapping(value = "/reviews/{username}")
    public @ResponseBody
    List<Review> getReviews(@PathVariable String username) {
        Session session = sessionFactory.getCurrentSession();
        return session
                .createQuery(
                        "select new edec.dto.ReviewDTO(r.userName,p.name,r.rating,r.review,r.reviewDate,r.productID) from Review r inner join r.product as p where r.userName = :username")
                .setParameter("username", username).list();
    }

    @RequestMapping(value = "/vote", method = RequestMethod.GET)
    public String vote(Model model, @RequestParam int id) {
        System.out.println(id);
        Product product = (Product) service.getEntity(Product.class, id);
        model.addAttribute("product", product);

        Review entityReview = new Review();
        entityReview.setProductID(id);

        model.addAttribute("reviewForm", entityReview);
        return "reviewProduct";
    }

    @Transactional
    @RequestMapping("/getRandomReview/{id}")
    public @ResponseBody
    List<Review> getRandomReviews(@PathVariable int id) {
        return service.getRandomReviews(id, 3);
    }

    @Transactional
    @RequestMapping(value = "/vote", method = RequestMethod.POST)
    public String votePost(@ModelAttribute("reviewForm") @Valid Review review, BindingResult bindingResult,
            Model model) {

        if (bindingResult.hasErrors()) {
            return "reviewProduct";
        } else {

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            review.setUserName(auth.getName());
            review.setReviewDate(new Date());

            Format formatter = new SimpleDateFormat("yyyy-MM-dd");
            String s = formatter.format(new Date());

            try {
                sessionFactory.getCurrentSession().createSQLQuery("INSERT INTO REVIEWS VALUES(?,?,?,?,?)")
                        .setParameter(0, review.getUserName())
                        .setParameter(1, review.getProductID())
                        .setParameter(2, review.getRating())
                        .setParameter(3, s)
                        .setParameter(4, review.getReview()).executeUpdate();

            } catch (Exception e) {
                model.addAttribute("message", "You already reviewed this product!");
                return "reviewProduct";
            }

            model.addAttribute("message", "Your review was successfully added!");

            return "index";
        }
    }

    @Transactional
    @RequestMapping(value = "/reviewProduct/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> review(@RequestBody ReviewForm form, @PathVariable int id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String s = formatter.format(new Date());

        Review review = new Review();
        review.setUserName(username);
        review.setProductID(id);
        review.setRating(form.getRating());
        review.setReview(form.getReview());
        review.setReviewDate(new Date());

        try {
            sessionFactory.getCurrentSession().createSQLQuery("INSERT INTO reviews VALUES(?,?,?,?,?)")
                    .setParameter(0, username)
                    .setParameter(1, id)
                    .setParameter(2, form.getRating())
                    .setParameter(3, s)
                    .setParameter(4, form.getReview()).
                    executeUpdate();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(review, HttpStatus.ACCEPTED);
    }

    @Transactional
    @RequestMapping(value = "/reviews/recommend/{username}")
    public @ResponseBody
    List<Review> recommendReviews(@PathVariable String username) {
        User user = (User) sessionFactory.getCurrentSession().createQuery("from User u where u.username = :user")
                .setParameter("user", username).uniqueResult();

        List<Integer> ids = new ArrayList<Integer>();
        for (Group g : user.getGroups()) {
            ids.add(g.getGroupid());

        }

        Session session = sessionFactory.getCurrentSession();
        List<String> results = new ArrayList<>();
        System.out.println("afxh : " + ids);

        results = session
                .createQuery(
                        "select distinct userName from GroupJoin g where g.groupid in (:list) and g.userName <> :usr ")
                .setParameter("usr", username).setParameterList("list", ids).list();
        List<Review> finalResult = new ArrayList<>();
        System.out.println("afxh2 : " + results);
        if (results.isEmpty()) {
            return null;
        }
        finalResult = session.createQuery("from Review r where r.rating > 3 and r.userName in :list").setParameterList("list", results)
                .list();

        return finalResult;
    }

    @Transactional
    @RequestMapping(value = "/recommendproducts")
    public String recommendProducts() {
        return "recommendproducts";
    }

    @RequestMapping(value = "/reviewStatistics/{id}")

    public @ResponseBody
    Map<Integer, String> reviewStatistics(@PathVariable int id) {

        return service.reviewStatistics(id);

    }
}
