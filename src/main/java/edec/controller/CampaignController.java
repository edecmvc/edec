package edec.controller;

import java.io.File;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edec.dao.GenericDAO;
import edec.model.Campaign;
import edec.model.CampaignJoin;
import edec.model.User;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import javax.servlet.ServletContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.ObjectError;

@Controller
public class CampaignController {

    private static final String PATH = "F:\\images\\";

    @Autowired
    GenericDAO<Campaign> service;
    @Autowired
    GenericDAO<CampaignJoin> dao;
    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    ServletContext context;

    @SuppressWarnings("unchecked")
    @RequestMapping("/rest/campaigns")
    @Transactional
    public @ResponseBody
    List<Campaign> viewCampaigns(@RequestParam(required = false) String keyword) {

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Campaign.class);

        if (keyword != null) {
            if (keyword.length() > 1) {
                Disjunction disjunction = Restrictions.disjunction();

                disjunction.add(Restrictions.ilike("creator", keyword, MatchMode.ANYWHERE));
                criteria.add(disjunction);

                if (criteria.list().size() == 0) {
                    disjunction.add(Restrictions.ilike("name", keyword, MatchMode.ANYWHERE));
                    criteria.add(disjunction);
                }
                if (criteria.list().size() == 0) {
                    disjunction.add(Restrictions.ilike("description", keyword, MatchMode.ANYWHERE));
                    criteria.add(disjunction);
                }
            } else {
                return criteria.list();
            }
        }
        return criteria.list();
    }

    @RequestMapping(value = "/addcampaign", method = RequestMethod.GET)
    public String addCampaign(Model model) {

        model.addAttribute("campaignForm", new Campaign());
        return "addcampaign";
    }

    @RequestMapping(value = "/addcampaign", method = RequestMethod.POST)
    public String addCampaignService(@ModelAttribute("campaignForm") @Valid Campaign campaign,
            BindingResult bindingResult, Model model) throws IOException {

        if (!bindingResult.hasErrors()) {

            if (service.isAlreadyTaken(campaign.getName())) {
                model.addAttribute("message", "A campaign with the chosen name already exists!");
                return "addcampaign";
            }

            //String username = SecurityContextHolder.getContext().getAuthentication().getName();
            //campaign.setCreatorUsername(username);
            File tmpFile = new File(context.getRealPath("") + "campaigns_data/tmp/avatar.png");

            if (tmpFile.exists()) {

                File finalDir = new File(context.getRealPath("") + "campaigns_data/" + campaign.getName());
                finalDir.mkdir();

                File finalFile = new File(finalDir.getAbsolutePath() + "/avatar.png");
                //finalFile.createNewFile();

                InputStream inputStream = new FileInputStream(tmpFile);
                OutputStream outputStream = new FileOutputStream(finalFile);

                byte[] buffer = new byte[1024];
                int length;

                while ((length = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, length);
                }

                while ((length = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, length);
                }
                inputStream.close();
                outputStream.close();

                tmpFile.delete();

                campaign.setIconPath("campaigns_data/" + campaign.getName() + "/avatar.png");
                //Files.move(tmpFile.toPath(), finalFile.toPath(), REPLACE_EXISTING);
            } else {
                campaign.setIconPath("campaigns_data/default/avatar.png");
            }

            Date date = new Date();
            campaign.setCreationDate(date);
            campaign.setcurrentJoins(0);
            String username = SecurityContextHolder.getContext().getAuthentication().getName();

            System.out.println("\n\n\n\nMy creator is: -" + username + "- this\n\n\n\n\n");

            campaign.setCreator(username);

            int id = service.saveEntity(campaign);

            return "redirect:/campaigns/" + id;
        }
        for (ObjectError it : bindingResult.getFieldErrors()) {
            System.out.println("\n\n\n\nDAAA\n\n\n\n\n");
            System.out.println(it.toString());
        }
        return "addcampaign";
    }

    @Transactional
    @RequestMapping(value = "/rest/campaigns/{username}")
    public @ResponseBody
    Set<Campaign> recommendGroups(@PathVariable String username) {

        User user = (User) sessionFactory.getCurrentSession().createQuery("from User u where u.username = :user")
                .setParameter("user", username).uniqueResult();
        return user.getJoinedCampaigns();

    }

    @RequestMapping(value = "/rest/joinCampaign", method = RequestMethod.POST)
    public ResponseEntity<?> joinCampaign(@RequestParam int id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        CampaignJoin campaignJoin = new CampaignJoin();
        campaignJoin.setCampaignId(id);
        campaignJoin.setUsername(username);

        dao.saveEntity(campaignJoin);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "/rest/leaveCampaign", method = RequestMethod.POST)
    public ResponseEntity<?> leaveCampaign(@RequestParam int id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        String sqlString = "DELETE from campaign_joins WHERE CampaignID = :cid and username = :user";
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sqlString);
        query.setParameter("cid", id);
        query.setParameter("user", username);
        int result = query.executeUpdate();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/unsubscribe/{id}", method = RequestMethod.DELETE)

    public ResponseEntity<?> unsubscribe(@PathVariable int id) {

        int count = service.unsubscribeUserFromCampaign(id);

        if (count == 0) {

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        } else {

            return new ResponseEntity<>(HttpStatus.ACCEPTED);

        }

    }
}
