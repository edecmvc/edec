package edec.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edec.model.Group;
import edec.model.User;

@Controller
public class StatisticsController {

    @Autowired
    SessionFactory sessionFactory;

    @Transactional
    @RequestMapping(value = "/rest/statistics/upvotes")
    public @ResponseBody
    List<Group> getUpVotes() {

        Session session = sessionFactory.getCurrentSession();

        List<Group> finalResult = new ArrayList<>();
        finalResult = session.createQuery("from Review group by productID  having avg(rating) > 3 order by rating desc").list();
        List<Group> resultLimited = new ArrayList<>();

        for (int i = 0; i < 5 && i < finalResult.size(); ++i) {
            resultLimited.add(finalResult.get(i));
        }
        if (resultLimited.size() == 0) {
            Group pam = new Group();
            pam.setName("Nothing");
            pam.setDescription("There are no products worth mentioning");
            resultLimited.add(pam);
        }
        return resultLimited;
    }
    
    @Transactional
    @RequestMapping(value = "/rest/statistics/downvotes")
    public @ResponseBody
    List<Group> getDownVotes() {

        Session session = sessionFactory.getCurrentSession();

        List<Group> finalResult = new ArrayList<>();
        finalResult = session.createQuery("from Review  group by productID  having avg(rating) < -3 order by rating").list();
        List<Group> resultLimited = new ArrayList<>();

        for (int i = 0; i < 5 && i < finalResult.size(); ++i) {
            resultLimited.add(finalResult.get(i));
        }
        if (resultLimited.size() == 0) {
            Group pam = new Group();
            pam.setName("Nothing");
            pam.setDescription("There are no products worth mentioning");
            resultLimited.add(pam);
        }
        return resultLimited;
    }
}
