package edec.controller;

import edec.dto.ProductDTO;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import edec.dao.GenericDAO;
import edec.model.Product;

import edec.model.Review;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletContext;
import org.hibernate.Session;

@Controller
public class ProductController {

    @Autowired
    SessionFactory sessionFactory;
    @Autowired

    GenericDAO service;
    
    @Autowired
        ServletContext context;

    @Transactional
    @RequestMapping("/products")
    public @ResponseBody
    List<Product> products() {
        return sessionFactory.getCurrentSession().createCriteria(Product.class).list();
    }

    @Transactional
    @RequestMapping("/rest/getproducts")
    public @ResponseBody
    Set<String> getProductsName() {
        Session session = sessionFactory.getCurrentSession();
        Set<Product> products = new HashSet(session.createCriteria(Product.class).list());

        Set<String> productsNames = new HashSet<String>();
        for (Product product : products) {
            productsNames.add(product.getName());

        }
        return productsNames;
    }

    @Transactional
    @RequestMapping(value = "/rest/addproduct", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<String> addProduct(@RequestBody @Valid Product product, BindingResult bindingResults) {

        String messages = "";
        for (ObjectError it : bindingResults.getFieldErrors()) {
            messages = messages + "/" + it.getDefaultMessage();
        }

        Product searchedProduct = (Product) sessionFactory.getCurrentSession().createQuery("from Product p where p.name = :productName").
                setParameter("productName", product.getName()).uniqueResult();

        if (searchedProduct != null) {
            messages = messages + "/" + "Product with the given name already exists! /";
        }

        if (bindingResults.getErrorCount() == 0) {
            sessionFactory.getCurrentSession().save(product);
            messages = "Product successfully added.";
            return new ResponseEntity<String>(messages, HttpStatus.OK);
        }

        return new ResponseEntity<>(messages, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/rest/newproduct", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<Object> addNewProduct(@RequestBody ProductDTO product) throws FileNotFoundException, IOException {

        Product productToBeInserted = new Product();

        productToBeInserted.setBrand(product.getBrand());
        productToBeInserted.setName(product.getName());
        productToBeInserted.setComposition(product.getComposition());
        productToBeInserted.setDescription(product.getDescription());
        productToBeInserted.setPrice(product.getPrice());
        
        File tmpFile = new File(context.getRealPath("") + "products_data/tmp/avatar.png");
               

                if(tmpFile.exists()){
                    
                    File finalDir = new File(context.getRealPath("") + "products_data/" + productToBeInserted.getName());
                    finalDir.mkdir();
                    
                    File finalFile = new File(finalDir.getAbsolutePath() + "/avatar.png");
                    //finalFile.createNewFile();
                    
                    InputStream inputStream = new FileInputStream(tmpFile);
                    OutputStream outputStream = new FileOutputStream(finalFile);
                    
                    byte[] buffer = new byte[1024];
                    int length;
                    
                    while((length = inputStream.read(buffer)) > 0){
                        outputStream.write(buffer, 0, length);
                    }
                    
                    while ((length = inputStream.read(buffer)) > 0){
                        outputStream.write(buffer, 0, length);
                    }
                    inputStream.close();
                    outputStream.close();
                    
                    tmpFile.delete();
                   
                    productToBeInserted.setIconPath("products_data/"+ productToBeInserted.getName() +"/avatar.png");
                    //Files.move(tmpFile.toPath(), finalFile.toPath(), REPLACE_EXISTING);
                }else{
                    productToBeInserted.setIconPath("products_data/default/avatar.png");
                }

        try {
            sessionFactory.getCurrentSession().save(productToBeInserted);
        } catch (Exception e) {
            sessionFactory.getCurrentSession().clear();
            return new ResponseEntity<Object>("Username is already taken!", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Object>(productToBeInserted, HttpStatus.ACCEPTED);

    }

    @Transactional
    @RequestMapping("/products/categories/{category}")
    public @ResponseBody
    List<Product> viewProductsCategories(@PathVariable String category) {
        return sessionFactory.getCurrentSession().createCriteria(Product.class).add(Restrictions.eq("category", category)).list();
    }

    @Transactional
    @RequestMapping("/products/categories")
    public @ResponseBody
    List<Product> viewCategories(@PathVariable String category) {
        return sessionFactory.getCurrentSession().createCriteria(Product.class).add(Restrictions.eq("category", category)).list();
    }

    @Transactional
    @RequestMapping(value = "/products/search")
    public @ResponseBody
    List<Product> search(@RequestParam String name, @RequestParam(required = false) String brand, @RequestParam(required = false) String description,
            @RequestParam(required = false) Double minprice, @RequestParam(required = false) Double maxprice, @RequestParam(required = false) String composition) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Product.class);
        if (name != null) {
            criteria.add(Restrictions.ilike("name", "%" + name + "%"));
        }

        if (brand != null) {
            criteria.add(Restrictions.ilike("brand", "%" + brand + "%"));
        }

        if (description != null) {
            criteria.add(Restrictions.ilike("description", "%" + description + "%"));
        }

        if (minprice != null) {
            criteria.add(Restrictions.ge("price", minprice));
        }

        if (maxprice != null) {
            criteria.add(Restrictions.le("price", maxprice));
        }

        if (composition != null) {
            criteria.add(Restrictions.ilike("composition", "%" + composition + "%"));
        }

        return criteria.list();
    }

    @Transactional
	@RequestMapping(value="/products/{id}")
	public String productView(@PathVariable int id,Model model){
		
		Product product = (Product) sessionFactory.getCurrentSession().createQuery("from Product p where p.id = :identifier")
				.setParameter("identifier", id).uniqueResult();
		
		model.addAttribute("reviewFlag",service.userReviewedProduct(id));
		model.addAttribute("reviewForm",new Review());
		model.addAttribute("targetID", id);
		model.addAttribute("product", product);
		return "productview";
	}

}
