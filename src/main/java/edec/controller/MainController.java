package edec.controller;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import edec.dao.GenericDAO;
import edec.dto.UserDTO;
import edec.model.Campaign;
import edec.model.Group;
import edec.model.User;
import edec.model.UserRole;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
public class MainController {

    @Autowired
    ServletContext context;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public ModelAndView defaultPage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("index");
        return model;

    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");

        return model;

    }

    // customize the error message
    private String getErrorMessage(HttpServletRequest request, String key) {

        Exception exception = (Exception) request.getSession().getAttribute(key);

        String error = "";
        if (exception instanceof BadCredentialsException) {
            error = "Invalid username and password!";
        } else if (exception instanceof LockedException) {
            error = exception.getMessage();
        } else {
            error = "Invalid username and password!";
        }

        return error;
    }

    // for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {

        ModelAndView model = new ModelAndView();

        // check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            System.out.println(userDetail);

            model.addObject("username", userDetail.getUsername());

        }

        model.setViewName("test");
        return model;

    }

    @Autowired
    GenericDAO service;

    @Autowired
    SessionFactory sessionFactory;

    @RequestMapping(value = "/myreviews", method = RequestMethod.GET)
    public String myReviews() {
        return "reviews";
    }

    @RequestMapping(value = "/campaigns", method = RequestMethod.GET)
    public String campaigns() {
        return "campaigns";
    }

    @Transactional

	@RequestMapping(value = "/campaigns/{campaignID}", method = RequestMethod.GET)
	public String singleCampaign(@PathVariable int campaignID, Model model) {
		
            Session session = sessionFactory.getCurrentSession();
            Campaign campaign = (Campaign) session.createQuery("from Campaign c where c.campaignID = :id")
                            .setParameter("id", campaignID).uniqueResult();

            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            if(!service.userHasJoined(username, campaignID)){
                    model.addAttribute("flag",true);
            }

            model.addAttribute("campaign", campaign);
            return "campaignview";
	}

    @RequestMapping("/register")
    public String register() {
        return "register";
    }
    
    @Transactional
    @RequestMapping("/users/{username}")
    public String currentUser(@PathVariable String username, Model model) {
        int result;
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.createQuery("from User u where u.username = :user").setParameter("user", username).uniqueResult();
        String query = "select count(campaignID) from campaign_joins where username = ?";
        result = new Integer(sessionFactory.getCurrentSession().createSQLQuery(query).setParameter(0, user.getUsername()).uniqueResult().toString());
                
        model.addAttribute("user", user);
        model.addAttribute("totalCampaigns", Integer.toString(result));
        
        query = "select count(ProductID) from reviews where username = ?";
        result = new Integer(sessionFactory.getCurrentSession().createSQLQuery(query).setParameter(0, user.getUsername()).uniqueResult().toString());
        
        model.addAttribute("totalProducts", Integer.toString(result));
        
        query = "select count(campaignID) from campaign_joins where username = ?";
        result = new Integer(sessionFactory.getCurrentSession().createSQLQuery(query).setParameter(0, user.getUsername()).uniqueResult().toString());
        model.addAttribute("totalGroups", Integer.toString(result));
        return "userview";
    }

    @RequestMapping(value = "/newuser", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<Object> addUser(@RequestBody UserDTO user) throws IOException {

        PasswordEncoder encoder = new BCryptPasswordEncoder();

        User userToBeRegistered = new User();
        userToBeRegistered.setUsername(user.getUsername());
        userToBeRegistered.setPassword(encoder.encode(user.getPassword()));
        userToBeRegistered.setRealName(user.getRealName());

        File tmpFile = new File(context.getRealPath("") + "users_data/tmp/avatar.png");

        File finalDir = new File(context.getRealPath("") + "users_data/" + user.getUsername());
        finalDir.mkdir();

        File finalFile = new File(finalDir.getAbsolutePath() + "/avatar.png");
        //finalFile.createNewFile();

        OutputStream outputStream = new FileOutputStream(finalFile);
        InputStream inputStream;

        byte[] buffer = new byte[1024];
        int length;

        if (tmpFile.exists()) {

            inputStream = new FileInputStream(tmpFile);

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            tmpFile.delete();

            userToBeRegistered.setAvatarPath("users_data/" + user.getUsername() + "/avatar.png");
            //Files.move(tmpFile.toPath(), finalFile.toPath(), REPLACE_EXISTING);
        } else {

            inputStream = new FileInputStream(new File(context.getRealPath("") + "users_data/default/avatar.png"));

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            tmpFile.delete();

            userToBeRegistered.setAvatarPath("users_data/" + user.getUsername() + "/avatar.png");
        }

        inputStream.close();
        outputStream.close();

        userToBeRegistered.setEnabled(true);

        Set<UserRole> roles = new HashSet<>();
        roles.add(new UserRole(userToBeRegistered, "ROLE_USER"));
        userToBeRegistered.setUserRole(roles);

        try {
            sessionFactory.getCurrentSession().save(userToBeRegistered);
        } catch (Exception e) {
            sessionFactory.getCurrentSession().clear();
            return new ResponseEntity<Object>("Username is already taken!", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Object>(userToBeRegistered, HttpStatus.ACCEPTED);

    }

    @RequestMapping(value = "/registeredusers")
    @Transactional
    public @ResponseBody
    Set<String> getRegisteredUsers() {

        Session session = sessionFactory.getCurrentSession();
        Set<User> users = new HashSet(session.createCriteria(User.class).list());

        Set<String> usernames = new HashSet<String>();
        for (User user : users) {
            usernames.add(user.getUsername());
        }
        return usernames;
    }

    @RequestMapping(value = "/users")
    @Transactional
    public @ResponseBody
    ResponseEntity<Set<User>> getUsers() {

        Session session = sessionFactory.getCurrentSession();
        Set<User> users = new LinkedHashSet(session.createCriteria(User.class).list());
        if (users.size() > 0) {
            return new ResponseEntity<Set<User>>(users, HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    int rowStart = 0;

    @Transactional
    @RequestMapping(value = "/groups/{username}")
    public @ResponseBody
    Set<Group> getGroups(@PathVariable String username) {
        System.out.println("here am i");

        User user = (User) sessionFactory.getCurrentSession().createQuery("from User u where u.username = :user")
                .setParameter("user", username).uniqueResult();

        //rowStart+=1;
        List<Group> allGroups = new ArrayList<>(user.getGroups());
        Set<Group> fewGroups = new HashSet<>();
        for (int i = rowStart; i < rowStart + 5 && i < allGroups.size(); ++i) {
            fewGroups.add(allGroups.get(i));
        }
        rowStart += 5;
        return fewGroups;
    }

    @Transactional
    @RequestMapping(value = "/groups/search/{groupid}")
    public String getGroupSearch(@PathVariable int groupid, Model model) {

        Group group = (Group) sessionFactory.getCurrentSession().createQuery("from Group g where g.groupid = :identifier")
                .setParameter("identifier", groupid).uniqueResult();

        model.addAttribute("group", group);
        return "groupview";
    }

    @RequestMapping(value = "/mygroups")
    public String groups() {
        rowStart = 0;
        return "groups";
    }
    

    @RequestMapping(value = "/search")
    public String search() {
        return "searchform";
    }

    @RequestMapping(value = "/addproduct")
    public String newProduct() {
        return "addproduct";
    }

    @RequestMapping(value = "/mycampaigns")
    public String myCampaigns() {
        return "mycampaigns";
    }

    @RequestMapping(value = "/groups")
    public String groupSearch() {
        return "searchgroups";
    }

    @Transactional
    @RequestMapping(value = "/rest/campaign/image_upload", method = RequestMethod.POST)
    public @ResponseBody
    String uploadCampaignImage(MultipartHttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException {

        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = request.getFile(itr.next());

        byte[] bytes = mpf.getBytes();
        File dir = new File(context.getRealPath("") + "campaigns_data/tmp");

        if (!dir.exists()) {
            dir.mkdir();
        }

        File serverFile = new File(dir.getAbsolutePath() + File.separator + "avatar.png");
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
        stream.write(bytes);
        stream.close();

        return "campaigns_data/tmp/avatar.png";
    }

    @Transactional
    @RequestMapping(value = "/rest/user/image_upload", method = RequestMethod.POST)
    public @ResponseBody
    String uploadUserImage(MultipartHttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException {

        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = request.getFile(itr.next());

        byte[] bytes = mpf.getBytes();
        File dir = new File(context.getRealPath("") + "users_data/tmp");

        if (!dir.exists()) {

            dir.mkdir();
        }

        File serverFile = new File(dir.getAbsolutePath() + File.separator + "avatar.png");
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
        stream.write(bytes);
        stream.close();

        return "users_data/tmp/avatar.png";
    }

    @Transactional
    @RequestMapping(value = "/rest/product/image_upload", method = RequestMethod.POST)
    public @ResponseBody
    String uploadProductImage(MultipartHttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException {

        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = request.getFile(itr.next());

        byte[] bytes = mpf.getBytes();
        File dir = new File(context.getRealPath("") + "products_data/tmp");

        if (!dir.exists()) {
            dir.mkdir();
        }

        File serverFile = new File(dir.getAbsolutePath() + File.separator + "avatar.png");
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
        stream.write(bytes);
        stream.close();

        return "products_data/tmp/avatar.png";
    }
}
