package edec.util;

import java.io.Serializable;

public class ReviewPK implements Serializable {

	private static final long serialVersionUID = -3503536869074705304L;
	protected int productID;
	protected int rating;

	public ReviewPK() {
	}

	public ReviewPK(int productID, int rating) {
		super();
		this.productID = productID;
		this.rating = rating;
	}

}
