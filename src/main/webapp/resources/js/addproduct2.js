function uploadFormData(){
	$('#result').html('');

	var oMyForm = new FormData();
	oMyForm.append("file", file2.files[0]);

	var token = $("input[name='_csrf']").val();
	var header = "X-CSRF-TOKEN";
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	$.ajax({
		url: 'http://localhost:8085/EDeC/rest/product/image_upload',
		data: oMyForm,
		dataType: 'text',
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data){
			$('#img-path').val(data);

			$('#campaign-img').css('background-image', 'none');
			$('#campaign-img').css({
				backgroundImage: 'url(' + data + ')',
				backgroundSize: 'cover'
			});
		}
	});
}

$(document).ready(function(){

	$('#campaign-img').click(function() {
		$('#file2').trigger('click');
	});

	$('#file2').change(function(event) {
		uploadFormData();
	});

	var productsList;
	
		$.ajax({
			'url' : 'http://localhost:8085/EDeC/rest/getproducts',
			'type' : 'GET',
			'success' : function(data) {
				productsList = data;
			}
		});

	$("#submitproduct").on('click', function() {

		$('.alert').css({
			display: 'none'
		});
		var ok = true;

		var obj = {
			"name" : $("#Name").val(),
			"description" : $("#Description").val(),
			"price" : $("#Price").val(),
			"composition" : $("#Composition").val(),
			"brand" : $("#Brand").val()

		};

		console.log(obj);

		var token = $("input[name='_csrf']").val();
		var header = "X-CSRF-TOKEN";
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});

		if(obj.name.length < 3){
			$("#name-alert").text("").append("<strong>ERROR! </strong>The product name should be at least 3 characters long.")
								.css({
									display: 'block'
								});
			ok = false;
		}

		for (i = 0; i < productsList.length; i++) {
			if (obj.name.toUpperCase() == productsList[i].toUpperCase()){
				$("#name-alert").text("").append("<strong>ERROR! </strong>The product already exists.")
								.css({
									display: 'block'
								});
				ok = false;
			}
		}

		if(obj.description.length < 5){
			$("#desc-alert").text("").append("<strong>ERROR! </strong>The product description should be at least 5 characters long.")
								.css({
									display: 'block'
								});
			ok = false;
		}

		if(isNaN(obj.price) || obj.description.length == 0){
			$("#price-alert").text("").append("<strong>ERROR! </strong>The product price should be numeric value.")
								.css({
									display: 'block'
								});
			ok = false;
		}

		if(obj.composition.length < 5){
			$("#comp-alert").text("").append("<strong>ERROR! </strong>The product composition should be at least 5 characters long.")
								.css({
									display: 'block'
								});
			ok = false;
		}

		if(obj.brand.length == 0){
			$("#brand-alert").text("").append("<strong>ERROR! </strong>The product brand should should not be null.")
								.css({
									display: 'block'
								});
			ok = false;
		}

		if(ok){
			$.ajax({
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json'
				},
				url : "http://localhost:8085/EDeC/rest/newproduct",
				type : "POST",
				success : function(response) {
					$("#final-alert-succ").css({
						display: 'block'
					});
					window.scrollTop(0);
				},
				data : JSON
						.stringify(obj),
				error : function(msg) {
					$("#final-alert-danger").css({
						display: 'block'
					});
				}
			});

			$.ajax({
			'url' : 'http://localhost:8085/EDeC/rest/getproducts',
			'type' : 'GET',
			'success' : function(data) {
				productsList = data;
			}
		});
		}
		else{
			$("#final-alert-danger").css({
						display: 'block'
					});
		}

	});
});