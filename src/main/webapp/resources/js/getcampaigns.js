

jQuery(document).ready(function($) {
	$.ajax({
	    'url' : 'http://localhost:8085/EDeC/rest/campaigns',
	    'type' : 'GET',
	    'success' : function(data) {
	    	var contor = 0;

	        console.log(data);

	        if($("#campaigns-container-home").length){
	        	jQuery.each(data, function(i, val) {
	        		if(contor == 5){
	        			return;
	        		}
	        		$("#campaigns-container-home").append(
	            	"<section class=\"campaign\" id=\"campaign-nr"+ val.campaignID +"\" >" + 
	            		"<div class=\"campaign-img\" style='background-image: url(\""+
						"/EDeC/campaigns_data/" + val.name + "/avatar.png\")' ></div>" +
	            		"<h1 class=\"title\">" + val.name + "</h1>" +
	            		"<a href=\"/EDeC/users/"+val.creator+"\"class=\"creator-icon\" >@" + val.creator + "</a>" +
	            		"<p class=\"mem-number\" >Already " + val.currentJoins + " members</p>" +
	            		"<div class=\"loading-bar\"></div>" +
	            		"<a href=\"http://localhost:8085/EDeC/campaigns/"+ val.campaignID +"\" class=\"link-to-campaign\">Find More...</a>"+
	            	"</section>"
	            	);
	            	var widthAux = val.currentJoins * 100 / val.targetJoins;
					$( '<style>#campaign-nr' + val.campaignID + ' .loading-bar:after { width: '+ widthAux +'%;}</style>' ).appendTo( "head" );
	            	$( '<style>#campaign-nr' + val.campaignID + ' .creator-icon:before{ background-image: url("/EDeC/users_data/'+val.creator+'/avatar.png"); }</style>' ).appendTo( "head" );
	            	contor++;
	        	});
	        }

	        if($("#campaigns-container").length){
	        	jQuery.each(data, function(i, val) {
	        		$("#campaigns-container").append(
	            	"<section class=\"campaign\" id=\"campaign-nr"+ val.campaignID +"\">" + 
	            		"<div class=\"campaign-img\"></div>" +
	            		"<p class=\"description\">" + val.description + "</p>" +
	            		"<h1 class=\"title\">" + val.name + "</h1>" +
	            		"<a href=\"http://localhost:8085/EDeC/campaigns/"+ val.campaignID +"\" class=\"link-to-campaign\">Find More...</a>"+
	            	"</section>"
	            	);
	        	});
	        }
	    }
    });
});
