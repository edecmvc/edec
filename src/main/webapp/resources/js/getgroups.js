jQuery(document).ready(function($) {
	$.ajax({
	    'url' : 'http://localhost:8085/EDeC/rest/groups/search',
	    'type' : 'GET',
	    'success' : function(data) {
	    	console.log(data);
	        if($("#groups-container").length){
	        	jQuery.each(data, function(i, val) {
	        		$("#groups-container").append(
	            	"<div class=\"this-group\" id=\"group-nr"+ val.groupid +"\" >" + 
	            		"<p class=\"top-group\">" + val.name + "</p>" +
	            		"<a href=\"/EDeC/users/"+val.creator+"\"class=\"creator-icon\" >@" + val.creator + "</a>" +
	            		"<p class=\"members-number\" >We are "+val.numberOfMembers+" members</p>" +
	            		"<a class=\"view-btn\" href=\"/EDeC/groups/search/"+ val.groupid+"\">View Group!</a>"+
	            	"</div>"
	            	);
	            	
	            	$( '<style>#group-nr' + val.groupid + ' .creator:after{ background-image: url("/EDeC/users_data/'+val.creator+'/avatar.png"); }</style>' ).appendTo( "head" );
	        	});
	        }
	    }
    });

    $("#keyword").on('keydown input',function(){
		var searchKey = $("#keyword").val();
		$.ajax({
			'url' : 'http://localhost:8085/EDeC/rest/groups/search?keyword='+searchKey,
			'type' : 'GET',
			'success' : function(data) {
				$("#groups-container").html("");
				if(data.length == 0){
					$(".alert-info").show();
				}else {
					$(".alert-info").hide();
					jQuery.each(data, function(i, val) {
		        		$("#groups-container").append(
		            	"<div class=\"this-group\" id=\"group-nr"+ val.groupid +"\" >" + 
		            		"<p class=\"top-group\">" + val.name + "</p>" +
		            		"<a href=\"/EDeC/users/"+val.creator+"\"class=\"creator-icon\" >@" + val.creator + "</a>" +
		            		"<p class=\"members-number\" >We are "+val.numberOfMembers+" members</p>" +
		            		"<a class=\"view-btn\" href=\"/EDeC/groups/search/"+ val.groupid+"\">View Group!</a>"+
		            	"</div>"
		            	);
			            	
		            	$( '<style>#group-nr' + val.groupid + ' .creator:after{ background-image: url("/EDeC/users_data/'+val.creator+'/avatar.png"); }</style>' ).appendTo( "head" );
			        	});
					}
			}
		});
	});
});
