$(document).ready(function() {
	$.ajax({
		'url' : 'http://localhost:8085/EDeC/rest/campaigns',
		'type' : 'GET',
		'success' : function(data) {
			
		
				jQuery.each(data, function(i, val) {
					if( $('#secret').val() == val.creator){
						$("#campaigns-container").append(
			            	"<section class=\"campaign\" id=\"campaign-nr"+ val.campaignID +"\" >" + 
			            		"<div class=\"campaign-img\" style='background-image: url(\""+
								"/EDeC/campaigns_data/" + val.name + "/avatar.png\")' ></div>" +
			            		"<h1 class=\"title\">" + val.name + "</h1>" +
			            		"<a href=\"/EDeC/users/"+val.creator+"\"class=\"creator-icon\" >@" + val.creator + "</a>" +
			            		"<p class=\"mem-number\" >Already " + val.currentJoins + " members</p>" +
			            		"<div class=\"loading-bar\"></div>" +
			            		"<a href=\"http://localhost:8085/EDeC/campaigns/"+ val.campaignID +"\" class=\"link-to-campaign\">Find More...</a>"+
			            	"</section>"
		        		);
					
		        	var widthAux = val.currentJoins * 100 / val.targetJoins;
					$( '<style>#campaign-nr' + val.campaignID + ' .loading-bar:after { width: '+ widthAux +'%;}</style>' ).appendTo( "head" );
		        	$( '<style>#campaign-nr' + val.campaignID + ' .creator-icon:before{ background-image: url("/EDeC/users_data/'+val.creator+'/avatar.png"); }</style>' ).appendTo( "head" );
				}
				});

			
		}
	});
	
	$("#keyword").on('keydown input',function(){
		var searchKey = $("#keyword").val();
		$.ajax({
		'url' : 'http://localhost:8085/EDeC/rest/campaigns?keyword='+searchKey,
		'type' : 'GET',
		'success' : function(data) {
			$("#campaigns-container").html("");
			if(data.length == 0){
				$(".alert-info").show();
			}else {
				$(".alert-info").hide();
			jQuery.each(data, function(i, val) {
				if( $('#secret').val() == val.creator){
				$("#campaigns-container").append(
		            	"<section class=\"campaign\" id=\"campaign-nr"+ val.campaignID +"\" >" + 
		            		"<div class=\"campaign-img\" style='background-image: url(\""+
							"/EDeC/campaigns_data/" + val.name + "/avatar.png\")' ></div>" +
		            		"<h1 class=\"title\">" + val.name + "</h1>" +
		            		"<a href=\"/EDeC/users/"+val.creator+"\"class=\"creator-icon\" >@" + val.creator + "</a>" +
		            		"<p class=\"mem-number\" >Already " + val.currentJoins + " members</p>" +
		            		"<div class=\"loading-bar\"></div>" +
		            		"<a href=\"http://localhost:8085/EDeC/campaigns/"+ val.campaignID +"\" class=\"link-to-campaign\">Find More...</a>"+
		            	"</section>"
	            	);
	            	var widthAux = val.currentJoins * 100 / val.targetJoins;
					$( '<style>#campaign-nr' + val.campaignID + ' .loading-bar:after { width: '+ widthAux +'%;}</style>' ).appendTo( "head" );
	            	$( '<style>#campaign-nr' + val.campaignID + ' .creator-icon:before{ background-image: url("/EDeC/users_data/'+val.creator+'/avatar.png"); }</style>' ).appendTo( "head" );

			}
			});

			}
		}
	});
		
	});

	$('#center').on('click', function(){
		$('#campaigns-container').css('display','none');
		$('#groups-container').css('display','none');
		$('.my-reviews').css('display','block');
	});

	$('#right').on('click', function(){
		$('#campaigns-container').css('display','none');
		$('#groups-container').css('display','block');
		$('.my-reviews').css('display','none');
		$.ajax({
	    'url' : 'http://localhost:8085/EDeC/rest/groups/search',
	    'type' : 'GET',
	    'success' : function(data) {
	    	console.log(data);
	        if($("#groups-container").length){
	        	jQuery.each(data, function(i, val) {
	        		if( $('#secret').val() == val.creator){
	        		$("#groups-container").append(
	            	"<div class=\"this-group\" id=\"group-nr"+ val.groupid +"\" >" + 
	            		"<p class=\"top-group\">" + val.name + "</p>" +
	            		"<p class=\"creator\" >Creator: <strong>@" + val.creator + "</strong></p>" +
	            		"<p class=\"members-number\" >We are "+val.numberOfMembers+" members</p>" +
	            		"<a class=\"view-btn\" href=\"/EDeC/groups/search/"+ val.groupid+"\">View Group!</a>"+
	            	"</div>"
	            	);
	            	
	            	$( '<style>#group-nr' + val.groupid + ' .creator:after{ background-image: url("/EDeC/users_data/'+val.creator+'/avatar.png"); }</style>' ).appendTo( "head" );
	        	}
	        	});
	        }
	    }
    });

    $("#keyword").on('keydown input',function(){
		var searchKey = $("#keyword").val();
		$.ajax({
			'url' : 'http://localhost:8085/EDeC/rest/groups/search?keyword='+searchKey,
			'type' : 'GET',
			'success' : function(data) {
				$("#groups-container").html("");
				if(data.length == 0){
					$(".alert-info").show();
				}else {
					$(".alert-info").hide();
					jQuery.each(data, function(i, val) {
						if( $('#secret').val() == val.creator){
		        		$("#groups-container").append(
		            	"<div class=\"this-group\" id=\"group-nr"+ val.groupid +"\" >" + 
		            		"<p class=\"top-group\">" + val.name + "</p>" +
		            		"<p class=\"creator\" >Creator: <strong>@" + val.creator + "</strong></p>" +
		            		"<p class=\"members-number\" >We are "+val.numberOfMembers+" members</p>" +
		            		"<a class=\"view-btn\" href=\"/EDeC/groups/search/"+ val.groupid+"\">View Group!</a>"+
		            	"</div>"
		            	);
			            	
		            	$( '<style>#group-nr' + val.groupid + ' .creator:after{ background-image: url("/EDeC/users_data/'+val.creator+'/avatar.png"); }</style>' ).appendTo( "head" );
			        	}
			        	});
					}
			}
		});
	});
	});
});
