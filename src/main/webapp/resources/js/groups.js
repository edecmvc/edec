$(document).ready(function(){
	var table =  $('#example').DataTable( {
	        "ajax": {
	            "url": "http://localhost:8085/EDeC/rest/groups",
	            "dataSrc": ""
	        },
	        "ordering": false,
	        "columns": [
	                    { "data": "name" },
	                    { "data": "description" },
	                    { "data": "creator" }
	                ],
	                "columnDefs": [
	                               {
	                                   // The `data` parameter refers to the data for the cell (defined by the
	                                   // `data` option, which defaults to the column being worked with, in
	                                   // this case `data: 0`.
	                                   "render": function ( data, type, row ) {
	                                     return "<a href='group/" + row["groupid"]+ " '>"+data+"</a>"; 
	                                   },
	                                   "targets": 0
	                               },
	                               { "visible": false,  "targets": [  ] }
	                           ]
	    } );
	
	$('#example th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
    
      table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).header() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
});