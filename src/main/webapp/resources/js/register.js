function uploadFormData(){
	$('#result').html('');

	var oMyForm = new FormData();
	oMyForm.append("file", file2.files[0]);

	var token = $("input[name='_csrf']").val();
	var header = "X-CSRF-TOKEN";
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	$.ajax({
		url: 'http://localhost:8085/EDeC/rest/user/image_upload',
		data: oMyForm,
		dataType: 'text',
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data){
			$('#img-path').val(data);

			$('#campaign-img').css('background-image', 'none');
			$('#campaign-img').css({
				backgroundImage: 'url(' + data + ')',
				backgroundSize: 'cover'
			});
			$('#actual-path').val(data);
		}
	});
}

$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 300);
    });
}

$(document).ready(function() {

	$('#campaign-img').click(function() {
		$('#file2').trigger('click');
	});

	$('#file2').change(function(event) {
		uploadFormData();
	});

	$('input').focusin(function(event) {
		$(this).next().fadeIn('fast').css("display","inline-block");
		$(this).next().next().fadeIn('fast').css("display","inline-block");
	});

	var userlist;	
	$.ajax({
		'url' : 'http://localhost:8085/EDeC/registeredusers',
		'type' : 'GET',
		'success' : function(data) {
			userlist = data;
		}
	});

	$("#send-btn").on('click', function(e) {
		
		

		var realname = $('#realname').val();
		var username = $("#username").val();
		var password = $("#password").val();
		var passwordcheck = $("#password2").val();
		var avatarPath = $("#actual-path").val();
		var contor = 0;
		var flag = true;

		var obj = {
			"realName" : realname,
			"username" : username,
			"password" : password,
			"enabled" : 1,
			"avatarPath": avatarPath
		};

		$('.alert-success').each(function(index, el) {
			if($(this).css('display') != 'none'){
				contor++;
			}
		});

		if(contor == 4){
			var token = $("input[name='_csrf']").val();
			var header = "X-CSRF-TOKEN";
			$(document).ajaxSend(function(e, xhr, options) {
				xhr.setRequestHeader(header, token);
			});

			for (i = 0; i < userlist.length; i++) {
				if (username.toUpperCase() == userlist[i].toUpperCase()){
					$('#taken-username').css('display', 'block');
					$('#menu').scrollView();
					flag = false;
					break;
				}
			}

			if(flag){
				$.ajax({
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json'
					},
					url : "http://localhost:8085/EDeC/newuser",
					type : "POST",
					success : function(response) {
						$('#success').css('display','block');
						$('#menu').scrollView();
						var delay = 3000; //Your delay in milliseconds
						setTimeout(function(){ 
							window.location = '/EDeC/login'; 
						}, delay);
					},
					data : JSON.stringify(obj)
				});
			}
		}

		

		/*
		if (password == passwordcheck) {

			if (username.length > 3) {

				if (password.length > 3) {
					$.ajax({
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'application/json'
						},
						url : "http://localhost:8085/EDeC/newuser",
						type : "POST",
						success : function(
								response) {
							alert(response);
						},
						data : JSON
								.stringify(obj),
						error : function(msg) {
							$("#message").html(msg.responseText + "<br />");
						}
					});
				} else {
					$("#message")
							.html(
									"Password must be at least 3 chars long.");
				}
			} else {
				$("#message")
						.html(
								"Username length must be at least 3.");
			}

		} else {
			$("#message")
					.html(
							"The passwords don't match! Try again");
		}*/
	});
});
