function uploadFormData(){
	$('#result').html('');

	var oMyForm = new FormData();
	oMyForm.append("file", file2.files[0]);

	var token = $("input[name='_csrf']").val();
	var header = "X-CSRF-TOKEN";
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	$.ajax({
		url: 'http://localhost:8085/EDeC/rest/campaign/image_upload',
		data: oMyForm,
		dataType: 'text',
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data){
			$('#img-path').val(data);

			$('#campaign-img').css('background-image', 'none');
			$('#campaign-img').css({
				backgroundImage: 'url(' + data + ')',
				backgroundSize: 'cover'
			});
		}
	});
}

$(document).ready(function(){

	$('#campaign-img').click(function() {
		$('#file2').trigger('click');
	});

	$('#file2').change(function(event) {
		uploadFormData();
	});	
})


