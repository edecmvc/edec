$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function(){
	
	handleProductSuccess = function(){
		$("#statusmessage").html("Product successfuly added!");
	}
	
	hadleProductFailure = function(){
		$("#statusmessage").html("Something went wrong! Please try again!");
	}
	
	$("#submitproduct").on("click",function(){
		
        console.log(JSON.stringify($('form').serializeObject()));
		
		var token = $("input[name='_csrf']").val();
		var header = "X-CSRF-TOKEN";
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
		
		$.ajax({
			headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
		    },
		    url: "http://localhost:8085/EDeC/rest/addproduct",
		    type: "POST",
		    success: function(response){
		    	console.log("success!");
		    },
		    data:JSON.stringify($('form').serializeObject()),
		    error: function(xhr, status, error) {
		        var tokens = xhr.responseText.split("/");
		        $("#errors").html("");
		        for( i = 1 ; i< tokens.length;i++){
		        	$("#errors").append(tokens[i]+"<br />"); 
		        }
		     }
		});
	});
})
