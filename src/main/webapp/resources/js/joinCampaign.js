$(document).ready(function(){
	$("#joinCampaign").click(function(){
		console.log(this);
		var token = $("input[name='_csrf']").val();
		var header = "X-CSRF-TOKEN";
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
		
		$.ajax({
			headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
		    },
		    url: "http://localhost:8085/EDeC/rest/joinCampaign?id="+$("#joinCampaign").val(),
		    type: "POST",
		    success: function(response){
		    	location.reload();
		    },
		    error: function(xhr, status, error) {
		      console.log(error);
		    }
		});
	});


	$("#leaveCampaign").click(function(){
		console.log(this);
		var token = $("input[name='_csrf']").val();
		var header = "X-CSRF-TOKEN";
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
		
		$.ajax({
			headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
		    },
		    url: "http://localhost:8085/EDeC/rest/leaveCampaign?id="+$("#leaveCampaign").val(),
		    type: "POST",
		    success: function(response){
		    	location.reload();
		    },
		    error: function(xhr, status, error) {
		      console.log(error);
		    }
		});
	});
});