$(document).ready(function(){
	
	$("#submitGroup").on('click',function(){
		var JSONObj = {"name":$("#groupName").val(),"description":$("#groupDescription").val()};
		console.log(JSONObj);
		
		var token = $("input[name='_csrf']").val();
		var header = "X-CSRF-TOKEN";
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
		
		$.ajax({
			headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
		    },
		    url: "http://localhost:8085/EDeC/rest/addGroup",
		    type: "POST",
		    success: function(response){
		    	$("#message").html("Group created!");
		    },
		    data: JSON.stringify(JSONObj),
		    error: function(xhr, status, error) {
		    	$("#message").html("Something went wrong! ");
		     }
		});
		
	});
});