

/* global tinyMCE */

$(document).ready(function () {

    $("#reviewButton").click(function () {
        var JSONObj = {"rating": $("#example").val(), "review": tinyMCE.get('reviewText').getContent()};
        console.log(JSONObj);

        var token = $("input[name='_csrf']").val();
        var header = "X-CSRF-TOKEN";
        $(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });

        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: "http://localhost:8085/EDeC/reviewProduct/" + $("#productID").val(),
            type: "POST",
            success: function (response) {
                $("#reviewForm").toggle(1500);
                console.log(response);
                setTimeout(function () {
                    $("#ajaxCallbackMessage").html("<b>Your review: " + response.review + "</b>");
                }, 1700);
            },
            data: JSON.stringify(JSONObj),
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        url: "http://localhost:8085/EDeC/getRandomReview/" + $("#productID").val(),
        type: "GET",
        success: function (data) {
            console.log(data);
            $.each(data, function (i, val) {
                
                $("#randomProductsContainer").append(' <img src=/EDeC/' + val.userImagePath + ' alt="Smiley face" height="10" width="10" id="userPhoto"> ' + "<b>" + val.userName + "</b>" + " reviewed: <blockquote>" + val.review + "</blockquote> on " + val.reviewDate + ' <br /> <div class="hr2"></div><br ');
            });

        },
        error: function (xhr, status, error) {

        }
    });

    setInterval(function () {
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: "http://localhost:8085/EDeC/getRandomReview/" + $("#productID").val(),
            type: "GET",
            success: function (data) {

                $("#randomProductsContainer").html("");
                $.each(data, function (i, val) {
                    $("#randomProductsContainer").append(' <img src=/EDeC/' + val.userImagePath + ' alt="Smiley face" height="10" width="10" id="userPhoto"> ' + "<b>" + val.userName + "</b>" + " reviewed: <blockquote>" + val.review + "</blockquote> on " + val.reviewDate + ' <br /> <div class="hr2"></div><br ');
                });

            },
            error: function (xhr, status, error) {
                alert(error);
            }
        });
    }, 30000);


});