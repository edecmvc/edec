<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search in our products</title>

	

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.css"/>
 <link href="resources/css/searchform.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.js"></script>
<link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">

		
		<link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="resources/css/default.css" rel="stylesheet" type="text/css">
<script>



$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function(){
    
        
        $("#input-form").hide();
        $("#advanced-search").click(function(){
            $("#input-form").toggle();
        });
	console.log(window.location.pathname );
	var table =  $('#example').DataTable( {
	        "ajax": {
	            "url": "http://localhost:8085/EDeC/products",
	            "dataSrc": ""
	        },
	        "columns": [
	                    { "data": "name" },
	                    { "data": "brand" },
                            { "data": "price" },
	                    
	                ],
	                "columnDefs": [
	                               {
	                                   // The `data` parameter refers to the data for the cell (defined by the
	                                   // `data` option, which defaults to the column being worked with, in
	                                   // this case `data: 0`.
	                                   "render": function ( data, type, row ) {
	                                       return "<a href='products/" + row["id"]+ " '>"+data+"</a>"; 
	                                   },
	                                   "targets": 0
	                               },
	                               { "visible": false,  "targets": [  ] }
	                           ]
	    } );
	
	$("#searchbutton").on('click',function(){
		console.log(JSON.stringify($('form').serializeObject()));
		$.ajax({
			'url' : 'http://localhost:8085/EDeC/products/search?'+$('form').serialize(),
			'type' : 'GET',
			'success' : function(data) {
					/*
					$("#searchresults").empty();
					jQuery.each(data, function(i, val) {
						$("#searchresults").append("<p>"+val.name +" " + val.brand +" "+ val.description +" "+ "</p>")
						$("#searchresults").append("<a href='/EDeC/products/"+val.id+"'>Product page</a>")
					});*/
					table.ajax.url( 'http://localhost:8085/EDeC/products/search?'+$('form').serialize()).load();	
			}
		});	
	});
        
});
</script>
</head>
<body>
    <div class="content-zone">
    <jsp:include page="template/header-document.jsp"/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <button type="button" id="advanced-search">Advanced search</button>
        
        <br/>
        <br/>
        <br/>
       <form id="input-form">
           
		<label class = "ignored" for="name">Name</label>
		<input type="text" name="name"/>
		<label class = "ignored" for="brand">Brand</label>
                <input type="text" name="brand"/> <br/>
		<label class = "ignored" for="description">Description</label>
		<input type="text" name="description"/>
		<label class = "ignored" for="minprice">Minimum price</label>
		<input type="text" name="minprice"/><br/>
                <label class = "ignored" for="maxprice">Maximum price</label>
		<input type="text" name="maxprice"/>
		<label class = "ignored" for="composition">Composition</label>
		<input type="text" name="composition"/><br/>
		
		<button type="button" id="searchbutton">Search</button>
		
		<div id = "searchresults">
		
		</div>
	</form>
        
    
        <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Brand</th>
                <th>Price</th>
                

            </tr>
        </thead>
        <br/>
        <br/>

    </table>
    <br /> <br />
    </div>
	
	<jsp:include page="template/footer-document.jsp"/>
	
</body>
</html>