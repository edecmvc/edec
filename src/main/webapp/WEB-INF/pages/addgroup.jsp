<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>EDeC - Create a Campaign</title>
        <link href="resources/css/addgroup.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script type="text/javascript" src="resources/tinymce/tinymce.min.js"></script>
        
        <link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">


        <link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
        <link href="resources/ico/favicon.ico"  rel="shortcut icon">
        <link href="resources/css/default.css" rel="stylesheet" type="text/css">
        <script>
            $(document).ready(function () {
               
                $("#submitGroup").on('click', function () {
                     var it = tinyMCE.get('contentid').getContent();
                    console.log(it);
                    var JSONObj = {"name": $("#groupName").val(), "description": it};
                    console.log(JSONObj);

                    var token = $("input[name='_csrf']").val();
                    var header = "X-CSRF-TOKEN";
                    $(document).ajaxSend(function (e, xhr, options) {
                        xhr.setRequestHeader(header, token);
                    });

                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        url: "http://localhost:8085/EDeC/rest/addGroup",
                        type: "POST",
                        success: function (response) {
                            $("#message").html("Group created!");
                        },
                        data: JSON.stringify(JSONObj),
                        error: function (xhr, status, error) {
                            $("#message").html("Something went wrong! ");
                        }
                    });

                });
            });
        </script>
    </head>
    <body >
        <div class="content-zone">
            <jsp:include page="template/header-document.jsp" />
            <form id="register-form" name='registerForm' method="POST" ng-app="" ng-init="name = ''">

                <p class="small-title big-title">Hello, complete these fields if you want to create a new group</p>
                <div class="hr hr2"></div>


                <p class="small-title set-margin">What's the group name?</p>
                <input class="auth-inputs" id="groupName" type="text" placeholder="group's name" name="name" ng-model="name"/>
                <div class="alert alert-info" ng-show="name.length < 6">
                    <strong>Info!</strong>
                    <span>Group's name should be at least 6 chars long.</span>
                </div>
                <div class="alert alert-success" ng-show="name.length >= 6" >
                    <strong>Success!</strong>
                    <span>The group's name is ok.</span>
                </div>
                <div class="hr2"></div>

                <p class="small-title set-margin">Here goes the description</p>
                <textarea id ="contentid" name="content" style="width:100%"></textarea>

                <div class="hr hr2"></div>

                <input type="submit" value="Create group" id="submitGroup"/>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

            </form>		
            <script>
                tinymce.init({
                    selector: "textarea",
                    theme: "modern",
                    height: "250",
                    plugins: [
                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "emoticons template paste textcolor colorpicker textpattern imagetools"
                    ],
                    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    toolbar2: "print preview media | forecolor backcolor emoticons",
                    image_advtab: true,
                    templates: [
                        {title: 'Test template 1', content: 'Test 1'},
                        {title: 'Test template 2', content: 'Test 2'}
                    ]
                });
            </script>




        </div>
        <jsp:include page="template/footer-document.jsp" />
    </body>
</html>