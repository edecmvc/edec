<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link href="resources/css/recommendedgroups.css" rel="stylesheet" type="text/css">
        <link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
        <link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">
        <link href="resources/ico/favicon.ico"  rel="shortcut icon">
        <link href="resources/css/default.css" rel="stylesheet" type="text/css">
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Your groups</title>

        <script>

            $(window).scroll(function () {

                if (parseInt($(window).scrollTop()) === $(document).height() - $(window).height()) {

                    $.ajax({
                        'url': 'http://localhost:8085/EDeC/groups/recommend/${pageContext.request.userPrincipal.name}',
                        'type': 'GET',
                        'success': function (data) {

                            jQuery.each(data, function (i, val) {
                                $("#recommendedgroups").append(
                                        "<p><b>Group name:</b>  " + val.name + " <br /> <b>Group description: </b>" + val.description + '</p><button id="joinGroup" value="' + val.groupid + '">Join</button><hr /><div class="hr2"></div>'
                                        );
                            });
                        }
                    });
                }
            });
            $.ajax({
                'url': 'http://localhost:8085/EDeC/groups/recommend/${pageContext.request.userPrincipal.name}',
                'type': 'GET',
                'success': function (data) {

                    jQuery.each(data, function (i, val) {

                        $("#recommendedgroups").append(
                                "<p><b>Group name:</b>  " + val.name + " <br /> <b>Group description: </b>" + val.description + '</p><button id="joinGroup" value="' + val.groupid + '">Join</button><hr /><div class="hr2"></div>'
                                );
                    });
                }
            });

        </script>
        <script>
            $(document).ready(function () {
                $("#joinGroup").on('click', function () {

                    var token = $("input[name='_csrf']").val();
                    var header = "X-CSRF-TOKEN";
                    $(document).ajaxSend(function (e, xhr, options) {
                        xhr.setRequestHeader(header, token);
                    });


                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        url: "http://localhost:8085/EDeC/joinGroup/" + $("#joinGroup").val(),
                        type: "POST",
                        success: function (response) {
                            $("#joinGroup").html('<a><span class="glyphicon glyphicon-ok"></span>Joined!</a>');
                        },
                        error: function (xhr, status, error) {

                        }
                    });
                });
            });
        </script>
    </head>
    <body><div class="content-zone">
            <jsp:include page="template/header-document.jsp"/>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/>


            <h3>Groups you may be interesed in </h3>
            <div class="hr2"></div>
            <div id="recommendedgroups"></div>
        </div>
        <jsp:include page="template/footer-document.jsp"/>      
    </body>
</html>