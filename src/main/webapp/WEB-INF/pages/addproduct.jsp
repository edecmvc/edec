<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="resources/js/addproduct2.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Add a new product</title>	    
	    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <link rel="shortcut icon" href="resources/ico/favicon.ico">
	    <link href="resources/css/addproduct.css" rel="stylesheet" type="text/css">
	    <link href="resources/css/default.css" rel="stylesheet" type="text/css">
	    <link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">
	    <link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
	</head>
	<body>

		<div class="content-zone">
			<jsp:include page="template/header-document.jsp" />
		
		<form id="create-form">

		  	<div class="alert alert-success" id="final-alert-succ">
  				<strong>Success!</strong> The product has been successfully inserted.
			</div>

			<div class="alert alert-danger" id="final-alert-danger">
				<strong>ERROR! </strong>Something went wrong, check the status messages
  			</div>
			<p class="small-title big-title">Add a product or a service...</p>
			<div class="hr hr2"></div>

			<p class="small-title set-margin">Insert a name for your product or service </p>
			<input type="text" id="Name" name="name"/> 
			<div class="alert alert-danger" id="name-alert">
  			</div>
			<div class="hr2"></div>

			<p class="small-title set-margin">You can choose an avatar for your campaign</p>
	        <div id="campaign-img" ></div>

	        <div class="hr2"></div>
			
			<p class="small-title set-margin">Describe in few words your product or service</p>
			<textarea id="Description" name="description"></textarea>
			<div class="alert alert-danger" id="desc-alert">
  			</div>
			<div class="hr2"></div>

			<p class="small-title set-margin">How expensive is it ? Insert below the price</p>
			<input type="text" id="Price" name="price">
			<div class="alert alert-danger" id="price-alert">
  			</div>
			<div class="hr2"></div>

			<p class="small-title set-margin">What's the composition of your product?(If is a service, let it null)</p>
			<textarea id="Composition" name="composition"></textarea> 
			<div class="alert alert-danger" id="comp-alert">
  			</div>
			<div class="hr2"></div>
			
			<p class="small-title set-margin">What's the Brand?</p>
			<input type="text" id="Brand"  name="brand"> 
			<div class="alert alert-danger" id="brand-alert">
  			</div>
			<div class="hr2"></div>
			
			
			


  			<button type="button" id="submitproduct">Add product</button>


		</form>

		<form id="form2" method="post" enctype="multipart/form-data">
				  <!-- File input -->    
				  <input name="file2" id="file2" type="file" accept=".jpg,.png" style="display:none" />
				</form>	
		
		
		</div>
		<jsp:include page="template/footer-document.jsp"/>
	</body>
</html>