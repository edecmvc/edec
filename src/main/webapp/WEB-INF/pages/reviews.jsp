<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Your reviews</title>
<script>
	
			$.ajax({
				'url' : 'http://localhost:8085/EDEC/reviews/${pageContext.request.userPrincipal.name}',
				'type' : 'GET',
				'success' : function(data) {
					console.log(data);
					jQuery.each(data, function(i, val) {
						$("#reviews").append(
								"<li>You reviewed " + val.name
										+ " with a rating of:" + val.rating
										+ ".<br />Review: " + val.review
										+ "</li><br />");
					});
				}
			});

	function formSubmit() {
		document.getElementById("logoutForm").submit();
	}
</script>
</head>
<body>
	<c:url value="/logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<h3>Reviews for the current user:
		${pageContext.request.userPrincipal.name} will appear here:</h3>
	<ul id="reviews">
	</ul>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<a>You are logged in as ${pageContext.request.userPrincipal.name}</a> | <a
			href="javascript:formSubmit()"> Logout</a>
	</c:if>
</body>
</html>