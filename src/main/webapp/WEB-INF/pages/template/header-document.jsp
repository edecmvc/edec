<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header>
    <div id="user-account">
        <div style="background-image: url('/EDeC/users_data/${pageContext.request.userPrincipal.name}/avatar.png')"></div>
        <div id="arrow-btn"></div>
        <nav>
            <a href="/EDeC/addcampaign">ADD CAMPAIGN</a>
            <a href="/EDeC/addproduct">ADD PRODUCT</a>
            <a href="/EDeC/addgroup">CREATE GROUP</a>
            <a href="/EDeC/recommendgroups">RECOMMENDED GROUPS</a>
            <a href="/EDeC/mygroups">JOINED GROUPS</a>
            <a href="/EDeC/recommendproducts">RECOMMENDED PRODUCTS</a>
            <a href="/EDeC/users/${pageContext.request.userPrincipal.name}">MY ACCOUNT</a> 
            <a href="javascript:formSubmit()"> LOGOUT</a>
        </nav>
        <div></div>
    </div>

    <c:url value="/logout" var="logoutUrl" />
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>

    <nav id="menu"> 
    <c:if test="${pageContext.request.userPrincipal.name != null}">
            <a href="javascript:formSubmit()"> LOGOUT</a>
            <script>
                document.getElementById('user-account').style.display = 'inline-block';
            </script>

    </c:if> 
    
    <a href="/EDeC">HOME</a>
    <a href="/EDeC/campaigns">CAMPAIGNS</a> 
    <a href="/EDeC/groups">GROUPS</a> 
    <a href="/EDeC/search">PRODUCTS</a>
    <c:if
        test="${pageContext.request.userPrincipal.name == null}">
        <a href="/EDeC/login">LOGIN</a>
        <a href="/EDeC/register">REGISTER</a>
    </c:if>
</header>
    
    
    

<div id="sigla-container">
    <div id="second-lines"></div>
    <div id="logo"></div>
    <h1 id="first-h">Ethical Decisions</h1>
    <h1 id="second-h">for Consummers</h1>
</div>


<script>
    $('#user-account div:last-child').click(function(){
        $('#user-account nav').slideToggle(150);
    });

    document.getElementById("user-account").children[3].innerHTML = "Hello, " + "${pageContext.request.userPrincipal.name}";

    function formSubmit() {
        document.getElementById("logoutForm").submit();
    }
</script>