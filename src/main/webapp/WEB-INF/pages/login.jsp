<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>EDeC - Login</title>
	    <link href="resources/css/login.css" rel="stylesheet" type="text/css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="resources/css/default.css" rel="stylesheet" type="text/css">
	</head>
	<body onload='document.loginForm.username.focus();'>
		<div class="content-zone">
			<jsp:include page="template/header-document.jsp" />

				<c:if test="${not empty error}">
					<div class="alert alert-danger" id="final-alert-danger">
						<strong>ERROR! </strong>${error}
		  			</div>

				</c:if>

				<form id="login-form" action="<c:url value='/login' />" name='loginForm' method="POST">

					<p class="small-title big-title">Hello, please login</p>
					<div class="hr hr2"></div>
					
					<p class="small-title set-margin">This goes your username</p>
			        <input class="auth-inputs" type="text" placeholder="username" name="username"/>

			        <div class="vr"></div>
			        <p class="join-text">You don't have an account yet? Join our community <a href="register">here</a></p>
					
					<p class="small-title set-margin">And this your password</p>
			        <input class="auth-inputs" type="password" placeholder="password" name="password" />
			        <div class=" hr hr2"></div>

			        <input type="submit" class="set-margin" value="Login" />

					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

	    		</form>			
		</div>
		<jsp:include page="template/footer-document.jsp" />
	</body>
</html>