<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>EDeC - Create a Campaign</title>
	    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <link href="resources/css/addcampaign.css" rel="stylesheet" type="text/css">
	    <script type="text/javascript" src="resources/tinymce/tinymce.min.js"></script>
		<script src="resources/js/addcampaign.js"></script>
		<link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">


		<link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="resources/css/default.css" rel="stylesheet" type="text/css">
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>
	<body >
		<div class="content-zone">
			<jsp:include page="template/header-document.jsp" />

				<form:form action="addcampaign?${_csrf.parameterName}=${_csrf.token}" commandName="campaignForm" id="register-form" name='campaignForm' method="POST" ng-app="" ng-init="name='asdasd'">

					<p class="small-title big-title">Hello, complete these fields if you want to create a new campaign</p>
					<div class="hr hr2"></div>
					

					<p class="small-title set-margin">What's the campaign name?</p>
			        <form:input path="name" class="auth-inputs" type="text" placeholder="campaign's name" name="name" ng-model="name"/>
			        <div class="alert alert-info" ng-show="name.length < 6">
  						<strong>Info!</strong>
  						<span>Campaign's name should bt atleast 6 chars long.</span>
					</div>
					<div class="alert alert-success" ng-show="name.length >= 6" >
  						<strong>Success!</strong>
  						<span>The campaign's name is ok.</span>
					</div>
			        <div class="hr2"></div>

			        <p class="small-title set-margin">You can choose an avatar for your campaign</p>
			        <div id="campaign-img" ></div>

			        <div class="hr2"></div>

			        <p class="small-title set-margin">Here goes the description</p>
			       	<form:textarea path="description" name="content" style="width:100%"></form:textarea>
					
					<div class="hr2"></div>

					<p class="small-title set-margin">How many people should join ths campaign?</p>
			       	<form:input path="targetJoins" class="auth-inputs" type="text" placeholder="People number" name="targetJoins" ng-model="targetJoins"/>
			        <div class="alert alert-info" ng-show="targetJoins.length < 6">
  						<strong>Info!</strong>
  						<span>There should be a target</span>
					</div>
					<div class="alert alert-success" ng-show="targetJoins.length >= 6" >
  						<strong>Success!</strong>
  						<span>The number is ok.</span>
					</div>
			        

			        <div class="hr hr2"></div>

			        <input type="submit" value="Create Campaign" id="send-btn"/>

					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

					<form:input type="hidden" path="creator" name="creator" value=""/>

	    		</form:form>		
	    		<form id="form2" method="post" enctype="multipart/form-data">
				  <!-- File input -->    
				  <input name="file2" id="file2" type="file" accept="image/*" style="display:none" />
				</form>

 				<script>
 					

					tinymce.init({
					    selector: "textarea",
					    theme: "modern",
					    height : "250",
					    plugins: [
					        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
					        "searchreplace wordcount visualblocks visualchars code fullscreen",
					        "insertdatetime media nonbreaking save table contextmenu directionality",
					        "emoticons template paste textcolor colorpicker textpattern imagetools"
					    ],
					    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
					    toolbar2: "print preview media | forecolor backcolor emoticons",
					    image_advtab: true,
					    templates: [
					        {title: 'Test template 1', content: 'Test 1'},
					        {title: 'Test template 2', content: 'Test 2'}
					    ]
					});			        	
		        </script>
		</div>
		<jsp:include page="template/footer-document.jsp" />
	</body>
</html>