<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>EDeC</title>
		<link href="resources/css/home.css" rel="stylesheet" type="text/css">
		<link href="resources/css/_campaigns.css" rel="stylesheet" type="text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="resources/css/default.css" rel="stylesheet" type="text/css">
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>

	<body>		
		<div class="content-zone">

		<!-- Here we add the header -->
		<jsp:include page="template/header-document.jsp" />

		<div id="bg"></div>

		<div id="description-container">
			<p>
				Ethics or simple honesty is the building blocks upon which our whole society is based, and business is a part of our society, and it's integral to the practice of being able to conduct business, that you have a set of honest standards.Ethics is knowing the difference between what you have a right to do and what is right to do. 
			</p>
		</div>

		<div id="services-container">

			<div class="service" id="products">
				<div id="products-logo"></div>
				<p>Search for Products and evaluate them.</p>
				<a href="/EDeC/search">Products</a>
			</div>

			<div class="service" id="groups">
				<div id="groups-logo"></div>
				<p>Help others to choose their products.</p>
				<a href="/EDeC/groups">Groups</a>
			</div>

			<div class="service" id="campaigns">
				<div id="campaigns-logo"></div>
				<p>Join Campaigns that suits your beliefs.</p>
				<a href="/EDeC/campaigns">Campaigns</a>
			</div>	

		</div>

		<div id="campaigns-container-home">
				<script type="text/javascript" src= "resources/js/getcampaigns.js"></script>
		</div>
		
		</div><!-- Here we close the content-zone-->

		<!-- Here we add the footer-->
		<jsp:include page="template/footer-document.jsp" />


		<c:url value="/logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>

		<script src="resources/js/fix-index.js"></script>
	</body>
</html>