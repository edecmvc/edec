<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>EDeC - Register</title>
	    <link href="resources/css/register.css" rel="stylesheet" type="text/css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	    <link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="resources/css/default.css" rel="stylesheet" type="text/css">
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="resources/js/register.js"></script>
	</head>
	<body onload='document.registerForm.realname.focus();'>
		<div class="content-zone">
			<jsp:include page="template/header-document.jsp" />

				<form id="register-form" name='registerForm' method="POST" ng-app="" ng-init="
					realname=''; 
					username='';
					password='';
					password2=''
				">
					<p class="small-title big-title">Hello and welcome stranger, complete the fields to register</p>
					<div class="alert alert-danger" id="taken-username">
						<strong>Error</strong> This username is already taken.
					</div>
					<div class="alert alert-success" id="success">
  						<strong>Success!</strong>
  						<span>Yout account was successfully created! You'll be redirected to login page in few seconds :)</span>
					</div>
					<div class="hr hr2"></div>
					

					<p class="small-title set-margin">What's your real name?</p>
			        <input class="auth-inputs" type="text" placeholder="real name" name="realname" ng-model="realname" id="realname"/>
			        <div class="alert alert-info" ng-show="realname.length < 2">
  						<strong>Info!</strong>
  						<span>Your name should be atleast 2 chars long.</span>
					</div>
					<div class="alert alert-success" ng-show="realname.length >= 2" >
  						<strong>Success!</strong>
  						<span>Your name is ok.</span>
					</div>
			        <div class="hr2"></div>
					

					<p class="small-title set-margin">How would you like to call ya' ?</p>
			        <input class="auth-inputs" type="text" placeholder="username" name="username" ng-model="username" id="username"/>
			        <div class="alert alert-info" ng-show="username.length < 4">
  						<strong>Info!</strong> Choose a minimum 4 chars nickname
					</div>
					<div class="alert alert-success" ng-show="username.length >= 4" >
  						<strong>Success!</strong>
  						<span>Your nickname is awsome.</span>
					</div>
			        <div class="hr2"></div>

			        <p class="small-title set-margin">You can choose an avatar for your account</p>
			        <div id="campaign-img" ></div>

			        <div class="hr2"></div>
					
					<input type="hidden" id="actual-path" value=""/>

			        <p class="small-title set-margin">Set please a password for your account</p>
			        <input class="auth-inputs" type="password" placeholder="password" name="password" ng-model="password" id="password"/>
			        <div class="alert alert-info" ng-show="password.length < 6">
  						<strong>Info!</strong> Your password should be atleast 6 chars long
					</div>
					<div class="alert alert-success" ng-show="password.length >= 6" >
  						<strong>Success!</strong>
  						<span>Your password is strong.</span>
					</div>
			        <div class="hr2"></div>


			        <p class="small-title set-margin">Retype please your password... just in case</p>
			        <input class="auth-inputs" type="password" placeholder="retype password" name="password2" ng-model="password2" id="password2" />
			        <div class="alert alert-info" ng-show="password != password2 || password2.length == 0">
  						<strong>Info!</strong> This password should be the same password as previous one
					</div>
					<div class="alert alert-success" ng-show="password == password2 && password.length > 0" >
  						<strong>Success!</strong>
  						<span>Both passwords are the same.</span>
					</div>
			        <div class="hr hr2"></div>

			        <input type="submit" value="Register" id="send-btn"/>

					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

	    		</form>		
	    		<form id="form2" method="post" enctype="multipart/form-data">
				  <!-- File input -->    
				  <input name="file2" id="file2" type="file" accept=".jpg,.png" style="display:none" />
				</form>	
		</div>
		<jsp:include page="template/footer-document.jsp" />
	</body>
</html>