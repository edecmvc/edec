<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"> </script>
    
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Search groups</title>
        <link href="resources/css/searchgroups.css" rel="stylesheet" type="text/css">
       <link href="resources/template/css/footer-document.css" rel="stylesheet" type="text/css">

		<link href="resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="resources/css/default.css" rel="stylesheet" type="text/css">
        <script src="/EDeC/resources/js/getgroups.js"></script>
    </head>
    <body>
        <div class="content-zone">
            <jsp:include page="template/header-document.jsp"/>
            <p class="big-title small-title set-margin">Hello, search the coolest groups...</p>

            <input type="text" id="keyword" name="keyword" placeholder="Type here..."/>

            <div class="hr2"></div>
            
            <div class="alert alert-info">
                <strong>Sorry!</strong> There are no such groups. Try again with others keywords. :)
            </div>


            <div id="groups-container">

            </div>
        </div>
        <jsp:include page="template/footer-document.jsp"/>
    </body>
</html>