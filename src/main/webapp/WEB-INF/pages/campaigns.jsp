<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <meta charset="utf-8">
	    <title>EDeC - Search for Campaigns</title>
	    <link href="/EDeC/resources/css/campaigns.css" rel="stylesheet" type="text/css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link href="/EDeC/resources/template/css/footer-document.css" rel="stylesheet" type="text/css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<link href="/EDeC/resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="/EDeC/resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="/EDeC/resources/css/default.css" rel="stylesheet" type="text/css">
		<link href="/EDeC/resources/css/_campaigns.css" rel="stylesheet" type="text/css">


		<script src="/EDeC/resources/js/campaigns.js"></script>
	</head>
	<body>
		<div class="content-zone">
			<jsp:include page="template/header-document.jsp" />

			<p class="big-title small-title set-margin">Cool, checkout the latest campaigns...</p>

			<input type="text" id="keyword" name="keyword" placeholder="Type here..."/>

			<div class="hr2"></div>
			
			<div class="alert alert-info">
				<strong>Sorry!</strong> There are no such campaigns. Try again with others keywords. :)
			</div>
			<div id="campaigns-container"></div>


		</div>
		<jsp:include page="template/footer-document.jsp" />
	</body>
</html>