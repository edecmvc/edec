<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link href="/EDeC/resources/css/groups.css" rel="stylesheet" type="text/css">
        <link href="/EDeC/resources/template/css/header-document.css" rel="stylesheet" type="text/css">
        <link href="/EDeC/resources/template/css/footer-document.css" rel="stylesheet" type="text/css">
        <link href="/EDeC/resources/ico/favicon.ico"  rel="shortcut icon">
        <link href="/EDeC/resources/css/default.css" rel="stylesheet" type="text/css">
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Your groups</title>

        <script>
            $.ajax({
                'url': 'http://localhost:8085/EDeC/groups/${pageContext.request.userPrincipal.name}',
                'type': 'GET',
                'success': function (data) {
                    console.log(data);

                    jQuery.each(data, function (i, val) {
                        $("#groups").append(
                                "<p><b>Group name:</b>  " + val.name + " <br /> <b>Group description: </b>" + val.description + '</p><hr /><div class="hr2"></div>'
                                );
                    });
                }
            });
            $(window).scroll(function () {
                //alert($(window).scrollTop() + " " );
                if (parseInt($(window).scrollTop()) === $(document).height() - $(window).height()) {

                    $.ajax({
                        'url': 'http://localhost:8085/EDeC/groups/${pageContext.request.userPrincipal.name}',
                        'type': 'GET',
                        'success': function (data) {
                            console.log(data);

                            jQuery.each(data, function (i, val) {
                                $("#groups").append(
                                        "<p><b>Group name:</b> <br/> " + val.name + " <br /> <b>Group description: </b> <br/>" + val.description + '</p><hr /><div class="hr2"></div>'
                                        );
                            });
                        }
                    });
                }
            });


        </script>

    </head>
    <body><div class="content-zone">
            <jsp:include page="template/header-document.jsp"/>
            <br/><br/><br/><br/><br/><br/><br/><br/>
            <p class="small-title set-margin">You joined the following groups</p>
            <br/> 
            <div class="hr hr2"></div>

            <div id="groups">
            </div>
            <br /> <br />
        </div>
        <jsp:include page="template/footer-document.jsp"/>      
    </body>
</html>