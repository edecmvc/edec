<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a new product</title>
</head>
<body><script src="resources/js/addproduct.js"></script>
	<form id="create-form">
	<h5 id="errors"></h5>
	<label>Nam123e22</label>
	<input type="text" id="Name" name="name"/> 
	<br /><br />
	<label>Descr123iption22</label>
	<input type="text" id="Description" name="description"> 
	<br /><br />
	<label>Pric123e22</label>
	<input type="text" id="Price" name="price"> 
	<br /><br />
	<label>Compos213ition2312</label>
	<input type="text" id="Composition" name="composition"/> 
	<br /><br />
	<label>Br12312and</label>
	<input type="text" id="Brand"  name="brand"> 
	<br /><br />
	<button type="button" id="submitproduct">Add product</button>
	</form>
	
	<h4 id="statusmessage"></h4>
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
</body>
</html>
