<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
         <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

	
    <head>
  		
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Search in our products</title>
        <script

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.css"/>
        <link href="/EDeC/resources/css/groupview.css" rel="stylesheet" type="text/css">
        <link href="/EDeC/resources/template/css/header-document.css" rel="stylesheet" type="text/css">
        <link href="/EDeC/resources/template/css/footer-document.css" rel="stylesheet" type="text/css">
        <link href="/EDeC/resources/ico/favicon.ico"  rel="shortcut icon">
        <link href="/EDeC/resources/css/default.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.js"></script>
		<script>
		$(document).ready(function(){
			$("#joinGroup").on('click',function(){
				
				var token = $("input[name='_csrf']").val();
				var header = "X-CSRF-TOKEN";
				$(document).ajaxSend(function(e, xhr, options) {
					xhr.setRequestHeader(header, token);
				});
				
				
				$.ajax({
					headers: { 
				        'Accept': 'application/json',
				        'Content-Type': 'application/json' 
				    },
				    url: "http://localhost:8085/EDeC/joinGroup/"+$("#joinGroup").val(),
				    type: "POST",
				    success: function(response){
				    	$("#joinGroup").html('<a><span class="glyphicon glyphicon-ok"></span>Joined!</a>');
				    },
				    error: function(xhr, status, error) {
				      	
				    }
				});
			});
		});
		</script>
   <html>
              <body>
            <div class="content-zone">
                <jsp:include page="template/header-document.jsp"/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                
                <div id="services-container">
                    <div class="service" id="products">
                        <div id="products-logo"></div>
                        <h1><b>${group.name}</b></h1>
                        <h4>by : ${group.creator}</h4>
                        <p>${group.description}</p>
                        <c:if test="${not empty flag}">
   							<a><span class="glyphicon glyphicon-ok"></span>Joined!</a>
						</c:if>
						  <c:if test="${empty flag}">
   							  <button id="joinGroup" value="${group.groupid}">Join</button>
						</c:if>
                      
                        <a href="/EDeC/groups">Back</a>
                        <br /> <br />
                    </div>
					<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                </div>

            </div>
            <jsp:include page="template/footer-document.jsp"/>      
        </body>
    </html>