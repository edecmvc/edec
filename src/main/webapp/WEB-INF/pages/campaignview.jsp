<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
            <base href="http:/localhost:8085/EDeC" target="_blank">
	    <meta charset="utf-8">
	    <title>${campaign.name}</title>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<link href="/EDeC/resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="/EDeC/resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="/EDeC/resources/css/default.css" rel="stylesheet" type="text/css">
		<link href="/EDeC/resources/template/css/footer-document.css" rel="stylesheet" type="text/css">
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script type="text/javascript" src="/EDeC/resources/js/joinCampaign.js"></script>
		<script type="text/javascript" src="/EDeC/resources/js/campaignview.js"></script>
                <script>
                    
                    $(document).ready(function(){
                       $.each($(".campaign-desc img"),function(i,val){
                          //console.log();
                          $(val).attr("src","/EDeC/"+$(val).attr('src'));
                       });
                    });
                </script>
		<link href="/EDeC/resources/css/campaignview.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="content-zone">
			<jsp:include page="template/header-document.jsp" />
			<p id="campaign-title">${campaign.name}</p>
			<div class="hr"></div>
			<div id="top-container">

				<div class="campaign-img" style='background-image: url("/EDeC/${campaign.iconPath}")'></div>

				<div class="details-container">
					
					<c:if test="${empty flag}">
					   		<p class="p-title">You are supporting this campaign. Thank you!</p>
					</c:if>
					<c:if test="${not empty flag}">
				    	<p class="p-title">You can join this campaign</p>
					</c:if>

					<p class="creator">Initiator: <strong>@${campaign.creator}</strong></p>
					<script>
						$( '<style>.creator:after { background-image: url("/EDeC/users_data/${campaign.creator}/avatar.png"); }</style>' ).appendTo( "head" );
						var widthAux = ${campaign.currentJoins} * 100 / ${campaign.targetJoins};
						$( '<style>.loading-bar:after { width: '+ widthAux +'%;}</style>' ).appendTo( "head" );
					</script>
					<p class="p-style">We are ${campaign.currentJoins} members</p>
					<div class="loading-bar"></div>
					<p class="p-style" id="last-p">${campaign.targetJoins - campaign.currentJoins } more members to reach the target
					</p>
						<c:if test="${empty flag}">
					   		<button type="button" id ="leaveCampaign" value="${campaign.campaignID}">Leave this campaign!</button>
						</c:if>
						<c:if test="${not empty flag}">
					    	<button type="button" id ="joinCampaign" value="${campaign.campaignID}">Join this campaign!</button>
						</c:if>
						<c:if test="${campaign.currentJoins >= campaign.targetJoins}">
					   		<button type="button" id ="leaveCampaign" value="${campaign.campaignID}">YAAAY!</button>
						</c:if>
				</div>
				<div class="hr"></div>
				<p id="title-desc">What is this campaign about?</p>
				<div class="campaign-desc">
					${campaign.description}
				</div>



			</div>


		</div>


		
		<jsp:include page="template/footer-document.jsp" />
	</body>
</html>