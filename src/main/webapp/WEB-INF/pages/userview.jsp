<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>EDeC - ${user.username}</title>
	    <link href="/EDeC/resources/css/userview.css" rel="stylesheet" type="text/css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link href="/EDeC/resources/template/css/footer-document.css" rel="stylesheet" type="text/css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<link href="/EDeC/resources/template/css/header-document.css" rel="stylesheet" type="text/css">
		<link href="/EDeC/resources/ico/favicon.ico"  rel="shortcut icon">
		<link href="/EDeC/resources/css/default.css" rel="stylesheet" type="text/css">
		<script src="/EDeC/resources/js/userview.js"></script>
		<link href="/EDeC/resources/css/_campaigns.css" rel="stylesheet" type="text/css">
		<link href="/EDeC/resources/css/searchgroups.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<input type="hidden" id="secret" value="${user.username}">
		<div class="content-zone">
			<jsp:include page="template/header-document.jsp" />
			<p class="user-title" >Hello, this is ${user.realName}</p>
			<div class="hr2"></div>
			<div class="info-container">
				<div id="user-avatar" style="background-image: url('/EDeC/${user.avatarPath}');"></div>
				<div id="stats">
					<p id="hello">Hello, this is <strong>@${user.username}</strong> and these are my stats.</p>
					<p class="stats" id="status-campanii">Total of ${totalCampaigns} added campaigns</p>
					<p class="stats" id="status-products">I did ${totalProducts} products reviews</p>
					<p class="stats" id="status-groups">I have created ${totalGroups} groups</p>
				</div>
			</div>
			<div class="hr2"></div>
			<div class="btn-container">
				<button id="left" type="button">Campaigns</button>
				<button id="center" type="button">Reviews</button>
				<button id="right" type="button">Groups</button>
			</div>
			<p class="big-title small-title" id="camp-title">Cool, checkout the news...</p>
			<input type="text" id="keyword" name="keyword" placeholder="Type here..."/>
			<div class="hr2"></div>

			<div id="campaigns-container">
				
			</div>

			<div id="groups-container">
				
			</div>


			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />	
		</div>
		<jsp:include page="template/footer-document.jsp" />
	</body>
</html>