<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>

        <title>Review product</title>
        <link href="../resources/css/productview.css" rel="stylesheet"
              type="text/css">

        <script src="resources/tinymce/tinymce.min.js"></script>
        <script>$(document).ready(function () {


                tinymce.init({
                    selector: 'textarea',
                    height: 150,
                    width: 800,
                    theme: 'modern',
                    plugins: [
                        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime media nonbreaking save table contextmenu directionality',
                        'emoticons template paste textcolor colorpicker textpattern imagetools'
                    ],
                    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    toolbar2: 'print preview media | forecolor backcolor emoticons',
                    image_advtab: true,
                    templates: [
                        {title: 'Test template 1', content: 'Test 1'},
                        {title: 'Test template 2', content: 'Test 2'}
                    ],
                    content_css: [
                        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                        '//www.tinymce.com/css/codepen.min.css'
                    ]
                });

                $("#example").css("position", "relative");
                $("#example").css("left", "25%");
            });
        </script>
    </head>
    <body>
        <h1>${message }</h1>
        <h4>You haven't reviewed this product.Tell us your opinion:</h4>
        <form:form commandName="reviewForm" action="vote">
            <label for="rating">Rating *:</label>
            <form:select id="example" path="rating" style="margin-left:25%;">
            <option value="1">Bad</option>
            <option value="2">Mediocre</option>
            <option value="3">Good</option>
            <option value="4">Awesome</option>
            <option value="5">Outstanding</option>
        </form:select><br />
        <form:errors path="rating"/>
        <br />
        <label for="review">Review *:</label>
        <form:textarea path="review" id="reviewText"/>
        <br />
        <form:errors path="review"/>
        <br /><br />
        <input type="button" id="reviewButton" value="Submit review"/>
        <form:input path="productID" type="hidden" value="${reviewForm.productID}"></form:input>
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
    </form:form>
</body>
</html>