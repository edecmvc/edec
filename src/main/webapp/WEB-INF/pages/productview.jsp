<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Search in our products</title>
    <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
          crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
          crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
    crossorigin="anonymous"></script>


    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.css" />
    <link href="../resources/css/productview.css" rel="stylesheet"
          type="text/css">
    <link href="../resources/template/css/header-document.css"
          rel="stylesheet" type="text/css">
    <link href="../resources/template/css/footer-document.css"
          rel="stylesheet" type="text/css">
    <link href="../resources/ico/favicon.ico" rel="shortcut icon">
    <link href="../resources/css/default.css" rel="stylesheet"
          type="text/css">
    <script type="text/javascript"
    src="https://cdn.datatables.net/u/dt/jq-2.2.3,dt-1.10.12/datatables.min.js"></script>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="../resources/jquery-bar-rating-master/dist/themes/bars-movie.css">
    <script
    src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script
    src="../resources/jquery-bar-rating-master/dist/jquery.barrating.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $('#example').barrating({
                theme: 'bars-movie',
                onSelect: function (value, text, event) {
                    console.log($("#example").val());
                }
            });
        });
    </script>
    <script src="../resources/tinymce/tinymce.min.js"></script>
    <script src="../resources/js/reviewproduct.js"></script>
    <script>
        tinymce
                .init({
                    selector: 'textarea',
                    height: 150,
                    width: 800,
                    theme: 'modern',
                    plugins: [
                        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime media nonbreaking save table contextmenu directionality',
                        'emoticons template paste textcolor colorpicker textpattern imagetools'],
                    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    toolbar2: 'print preview media | forecolor backcolor emoticons',
                    image_advtab: true,
                    templates: [{
                            title: 'Test template 1',
                            content: 'Test 1'
                        }, {
                            title: 'Test template 2',
                            content: 'Test 2'
                        }],
                    content_css: [
                        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                        '//www.tinymce.com/css/codepen.min.css']
                });
    </script>
    <script>

        $(document).ready(function () {

            $.ajax({
                url: "http://localhost:8085/EDeC/reviewStatistics/1",
                type: "GET",
                success: function (data) {



                    var totalReviews = 0;

                    $.each(data, function (i, val) {

                        totalReviews = totalReviews + Number(val);

                    });



                    console.log(totalReviews);



                    $.each(data, function (i, val) {

                        var percent = Math.floor((Number(val) / totalReviews) * 100);

                        var stripeHTML = '<div class="progress"><div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:' + percent + '%">' + percent + '%</div></div>';

                        $("#content").append(i + " stars:" + stripeHTML);

                    });

                }

            });

        });

    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
    <div class="content-zone">
        <jsp:include page="template/header-document.jsp" />
        <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
        <input type="text" id="productID" style="display: none"
               value="${targetID}" />
        <div id="content" style="width:15%;">Number of votes : <br/><br/></div>
        <div id="image" style="background-image: url('/EDeC/${product.iconPath}'); background-size: content; background-position: center;"></div>
        <div id="services-container">
            <div class="service" id="products">
                <div id="inside">
                    <div id="products-logo"></div>
                    <h1>
                        <b>${product.name}</b>
                    </h1>
                    <br/>
                    <p>by : ${product.brand}</p>

                    <p>Product composition : ${product.composition}</p>
                    <p>Product price : ${product.price}</p>
                    <a href="../search">Back</a> <br /> <br />
                </div>
            </div>
            <p id = "description">${product.description}</p>
            <div class="hr2"></div>
            <div id="review">
                <c:choose>
                    <c:when test="${reviewFlag eq true}">
                        <h3>You already reviewed the product!</h3>
                        <br />
                    </c:when>
                    <c:otherwise>
                        <div id="reviewForm">
                            <jsp:include page="reviewProduct.jsp" />
                            <input type="hidden" name="${_csrf.parameterName}"
                                   value="${_csrf.token}" />
                        </div>
                        <br />
                    </c:otherwise>
                </c:choose>
            </div>
            <div id="ajaxCallbackMessage"></div>
            <div class="hr2"></div>
           
            <div id="randomProductsContainer"></div>
        </div>

    </div>



    <jsp:include page="template/footer-document.jsp" />
</body>
</html>
